using Hightlander.Pong;
using Hightlander.Pong.Gameplay;
using Hightlander.Pong.HighScore;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;

/// <summary>
///     ����������� ������� �����������.
/// </summary>
public class GameComponentsInstaller : MonoInstaller
{
    [SerializeField, UsedImplicitly(ImplicitUseKindFlags.Assign)]
    private BallManagerConfiguration BallManagerConfiguration;

    [SerializeField, UsedImplicitly(ImplicitUseKindFlags.Assign)]
    private ScoreTracker ScoreTracker;

    [SerializeField, UsedImplicitly(ImplicitUseKindFlags.Assign)]
    private float NetworkPositionEpsilon = 0.001f;

    public override void InstallBindings()
    {
        Container.Bind<float>().WithId("network-position-epsilon").FromInstance(NetworkPositionEpsilon).AsTransient();

        Container.Bind<Playfield>().ToSelf().FromNew().AsSingle();
        Container.Bind<SessionController>().ToSelf().AsSingle();
        Container.Bind<ScoreTracker>().ToSelf().FromInstance(ScoreTracker);
        Container.Bind<BallManagerConfiguration>().ToSelf().FromInstance(BallManagerConfiguration);
    }
}