﻿using Hightlander.Pong.Gameplay;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace Hightlander.Pong
{
    /// <summary>
    ///     Контроллер внутриигрового меню.
    /// </summary>
    public sealed class InGameMenuController : MonoBehaviour
    {
        [Inject, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private SessionController Session;

        /// <summary>
        ///     Перезапустить сессию.
        /// </summary>
        [PublicAPI]
        public void RestartSession()
        {
            Session.End();
            Session.Start();
        }

        /// <summary>
        ///     Переключить паузу.
        /// </summary>
        [PublicAPI]
        public void TogglePause()
        {
            Session.IsSessionActive = !Session.IsSessionActive;
        }
    }
}