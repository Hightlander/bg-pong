﻿using System;
using System.Threading;
using Hightlander.Pong.Gameplay;
using Hightlander.Pong.Networking;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using Zenject;

namespace Hightlander.Pong
{
    /// <summary>
    ///     Контроллер меню приложения.
    /// </summary>
    public class MenuController : MonoBehaviour
    {
        [SerializeField, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private TextMeshProUGUI TextArea;

        [SerializeField, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private TMP_Text ConnectionInfoLabel;

        [SerializeField, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private GameConfigurator GameConfigurator;

        [SerializeField, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private Animator UIAnimator;

        [Header("Triggers")]
        [SerializeField, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private string MainMenuTriggerName = "Menu";

        [SerializeField, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private string PlaymodeTriggerName = "Playfield";

        [Header("Settings")]
        [SerializeField, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private bool MockMultiplayer;

        [Inject, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private SessionController Session;

        private IPongClient Client;
        private IPongServer Server;

        private void OnDestroy()
        {
            Client?.Dispose();
            Server?.Dispose();
        }

        /// <summary>
        ///     Стартовать сервер многопользовательской игры.
        /// </summary>
        [UsedImplicitly]
        public void HostServer()
        {
            HostServerAsync();
        }

        private async void HostServerAsync()
        {
            if (MockMultiplayer)
            {
                Server = new MockPongServer();
            }
            else
            {
                Server = new PongServer();
            }

            Server.Run();

            ConnectionInfoLabel.text = "Ожидание подключения пользователей...";

            // Time limit - two minutes.
            bool PlayerConnected = await Server.WaitForAnotherPlayer(new CancellationTokenSource(2 * 60 * 1000).Token);

            if (PlayerConnected)
            {
                UIAnimator.SetTrigger(PlaymodeTriggerName);

                Session.SetMultiplayerType(true);
                GameConfigurator.ConfigureForMultiplayer(Server);
                Session.Start();
            }
            else
            {
                Debug.LogError("Timeout. No players connected.");
                UIAnimator.SetTrigger(MainMenuTriggerName);
            }

            Server.PlayerDisconnectedEvent += PlayerDisconnectedEventHandler;
        }

        /// <summary>
        ///     Подключиться к серверу.
        /// </summary>
        [UsedImplicitly]
        public void ConnectToServer()
        {
            string Ip = TextArea.text.Trim('​');
            Debug.Log($"Connection target: [{Ip}]");

            ConnectToServerAsync(Ip);
        }

        private async void ConnectToServerAsync([NotNull] string ip)
        {
            if (ip == null)
            {
                throw new ArgumentNullException(nameof(ip));
            }

            if (MockMultiplayer)
            {
                Client = new MockPongClient();
            }
            else
            {
                Client = new PongClient();
            }

            ConnectionInfoLabel.text = "Ожидание подключения к серверу...";

            bool ConnectionSucceded = await Client.Connect(ip);

            if (ConnectionSucceded)
            {
                UIAnimator.SetTrigger(PlaymodeTriggerName);
                Session.SetMultiplayerType(false);
                GameConfigurator.ConfigureForMultiplayer(Client);
                Session.Start();

                Client.ConnectionLostEvent += ConnectionLostEventHandler;
            }
            else
            {
                Debug.LogError("Failed to connect.");
                UIAnimator.SetTrigger(MainMenuTriggerName);
            }
        }

        /// <summary>
        ///     Поставить игровую сессию на паузу.
        /// </summary>
        [UsedImplicitly]
        public void PauseSession()
        {
            Session.IsSessionActive = false;
        }

        /// <summary>
        ///     Продолжить игровую сессию.
        /// </summary>
        [UsedImplicitly]
        public void ResumeSession()
        {
            Session.IsSessionActive = true;
        }

        /// <summary>
        ///     Завершить сессию.
        /// </summary>
        [UsedImplicitly]
        public void StopSession()
        {
            Session.End();
        }

        /// <summary>
        ///     Начать одиночную игру.
        /// </summary>
        [UsedImplicitly]
        public void StartSingleplayer()
        {
            GameConfigurator.ConfigureForSingleplayer();
            Session.SetSingleplayerType().Start();
        }

        private void PlayerDisconnectedEventHandler()
        {
            Server.PlayerDisconnectedEvent -= PlayerDisconnectedEventHandler;

            StopSession();

            Server.Stop();
            Server.Dispose();
            Server = null;

            UIAnimator.SetTrigger(MainMenuTriggerName);
        }

        private void ConnectionLostEventHandler()
        {
            Client.ConnectionLostEvent -= ConnectionLostEventHandler;
            Client.Dispose();
            Client = null;
            
            StopSession();
            UIAnimator.SetTrigger(MainMenuTriggerName);
        }
    }
}