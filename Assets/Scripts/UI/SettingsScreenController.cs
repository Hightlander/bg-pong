﻿using Hightlander.Pong.GameSettings;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Hightlander.Pong
{
    /// <summary>
    ///     Контроллер экрана настроек.
    /// </summary>
    [UsedImplicitly(ImplicitUseKindFlags.InstantiatedNoFixedConstructorSignature)]
    public sealed class SettingsScreenController : MonoBehaviour
    {
        [Inject, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private IBallColorSettings BallColorSettings;

        [Header("Components")]
        [SerializeField, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private Image BallColorPreview;

        [SerializeField, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private Slider RedComponentSlider;

        [SerializeField, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private Slider GreenComponentSlider;

        [SerializeField, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private Slider BlueComponentSlider;

        /// <summary>
        ///     Сохранить настройки.
        /// </summary>
        [PublicAPI]
        public void Save()
        {
            BallColorSettings.BallColor = new Color(RedComponentSlider.value, GreenComponentSlider.value, BlueComponentSlider.value);
        }

        /// <summary>
        ///     Загрузить сохранённые настройки.
        /// </summary>
        [PublicAPI]
        public void LoadSettingsData()
        {
            RedComponentSlider.value = BallColorSettings.BallColor.r;
            GreenComponentSlider.value = BallColorSettings.BallColor.g;
            BlueComponentSlider.value = BallColorSettings.BallColor.b;
        }

        private void Update()
        {
            BallColorPreview.color = new Color(RedComponentSlider.value, GreenComponentSlider.value, BlueComponentSlider.value);
        }
    }
}