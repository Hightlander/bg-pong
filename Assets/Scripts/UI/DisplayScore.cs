﻿using Hightlander.Pong.HighScore;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using Zenject;

namespace Hightlander.Pong
{
    /// <summary>
    ///     Компонент отображения счёта.
    /// </summary>
    public sealed class DisplayScore : MonoBehaviour
    {
        [SerializeField, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private TMP_Text Label;

        [Inject, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private ScoreTracker ScoreTracker;

        private void Update()
        {
            Label.text = $"{ScoreTracker.CurrentScore}\nHigh score: {ScoreTracker.HighScore}";
        }
    }
}