﻿using JetBrains.Annotations;
using UnityEngine;

namespace Hightlander.Pong
{
    public sealed class KeepCanvasScreenSize : MonoBehaviour
    {
        [SerializeField, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        public float DistanceToCamera = 10f;

        private RectTransform Transform;

        private void Awake()
        {
            Transform = GetComponent<RectTransform>();
        }

        private void Update()
        {
            Transform.sizeDelta = new Vector2(Screen.width, Screen.height);

            Transform.position = Camera.main.transform.position + (Camera.main.transform.forward * DistanceToCamera);

            float CameraHeight;
            if (Camera.main.orthographic)
            {
                CameraHeight = Camera.main.orthographicSize * 2;
            }
            else
            {
                CameraHeight = 2 * DistanceToCamera * Mathf.Tan(Mathf.Deg2Rad * (Camera.main.fieldOfView * .5f));
            }

            Transform.localScale = new Vector3(CameraHeight / Screen.height, CameraHeight / Screen.height, 1);
        }
    }
}