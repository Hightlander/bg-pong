﻿using Hightlander.Pong.Gameplay;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace Hightlander.Pong
{
    /// <summary>
    ///     Менеджер, управляющий появлением и удалением мячей в одиночной игре.
    /// </summary>
    [UsedImplicitly(ImplicitUseKindFlags.InstantiatedWithFixedConstructorSignature)]
    public sealed class SingleplayerBallManager : BasicBallManager, ITickable
    {
        [Inject, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private TickableManager TickableManager;

        protected override void Configure()
        {
            base.Configure();
            TickableManager.Add(this);
            Playfield.BallOutOfBoundsEvent += BallOutOfBoundEventHandler;
        }

        protected override void SessionStartedEventHandler()
        {
            if (Playfield.BallsCount <= 0)
            {
                SpawnBall();
            }
        }

        protected override void SessionEndedEventHandler()
        {
            foreach (Ball Ball in Playfield.GetBalls())
            {
                Playfield.RemoveBall(Ball);
            }
        }

        private void BallOutOfBoundEventHandler([NotNull] Ball ball)
        {
            Playfield.RemoveBall(ball);
        }

        #region Implementation of ITickable

        /// <inheritdoc />
        public void Tick()
        {
            if (Input.GetKeyDown(KeyCode.B))
            {
                SpawnBall();
            }
        }

        #endregion

        #region Overrides of BallManager

        /// <inheritdoc />
        public override void Dispose()
        {
            base.Dispose();

            TickableManager.Remove(this);
            TickableManager = null;

            Playfield.BallOutOfBoundsEvent -= BallOutOfBoundEventHandler;
        }

        #endregion
    }
}