﻿using JetBrains.Annotations;
using UnityEngine;

namespace Hightlander.Pong
{
    /// <summary>
    ///     Контроллер параллакса фона во время игры.
    /// </summary>
    public sealed class BackgroundParallax : MonoBehaviour
    {
        [SerializeField, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private float ParallaxMagnitude = 10f;

        private void Update()
        {
            Vector2 InputPosition = Input.mousePosition;

            if (Input.touchSupported && Input.touchCount > 0)
            {
                var Touch = Input.GetTouch(0);

                InputPosition = Touch.position;
            }

            InputPosition = new Vector2(InputPosition.x - Screen.width / 2f, InputPosition.y - Screen.height / 2f);
            transform.localPosition = new Vector3(Mathf.Clamp(InputPosition.x / Screen.width, -1, 1) * ParallaxMagnitude, Mathf.Clamp(InputPosition.y / Screen.height, -1, 1) * ParallaxMagnitude, 0);
        }
    }
}