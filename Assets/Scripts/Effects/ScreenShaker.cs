﻿using Hightlander.Pong.Gameplay;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace Hightlander.Pong
{
    /// <summary>
    ///     Контроллер трясучки экрана.
    /// </summary>
    public sealed class ScreenShaker : MonoBehaviour
    {
        // По хорошему обработку событий и вызовы тряски нужно вынести в отдельный компонент, этот, в свою очередь, использовать через DI.
        // Но в этой работе не будем плодить сущности.

        [SerializeField, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private Camera TargetCamera;

        [SerializeField, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private float DefaultShakeMagnitude = 0.1f;

        [SerializeField, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private float DampingPower = 0.5f;

        [Inject, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private Playfield Playfield;

        private Vector3 InitialCameraPosition;

        private void Awake()
        {
            InitialCameraPosition = TargetCamera.transform.position;

            Playfield.BallSpawnedEvent += BallSpawnedEventHandler;
            Playfield.BallRemovedEvent += BallRemovedEventHandler;
        }

        private void OnDestroy()
        {
            Playfield.BallSpawnedEvent -= BallSpawnedEventHandler;
            Playfield.BallRemovedEvent -= BallRemovedEventHandler;
        }

        private void Update()
        {
            if (Vector3.Distance(TargetCamera.transform.position, InitialCameraPosition) > 0.0001f)
            {
                TargetCamera.transform.position =
                    Vector3.Lerp(TargetCamera.transform.position, InitialCameraPosition, DampingPower);
            }
            else
            {
                TargetCamera.transform.position = InitialCameraPosition;
            }
        }

        [PublicAPI]
        public void ShakeIt()
        {
            ShakeIt(DefaultShakeMagnitude);
        }

        [PublicAPI]
        public void ShakeIt(float shakeMagnitude)
        {
            TargetCamera.transform.position += new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f)).normalized * shakeMagnitude;
        }

        private void BallSpawnedEventHandler(Ball ball)
        {
            ball.BorderCollisionEvent += BallCollisionEventHandler;
            ball.PlankCollisionEvent += PlankCollisionEventHandler;
        }

        private void BallRemovedEventHandler(Ball ball)
        {
            ball.BorderCollisionEvent -= BallCollisionEventHandler;
            ball.PlankCollisionEvent -= PlankCollisionEventHandler;
        }

        private void PlankCollisionEventHandler(Ball ball, Plank plank)
        {
            ShakeIt();
        }

        private void BallCollisionEventHandler(Ball ball)
        {
            ShakeIt();
        }
    }
}