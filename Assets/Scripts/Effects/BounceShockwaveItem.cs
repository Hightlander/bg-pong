﻿using System.Threading.Tasks;
using Hightlander.Pong.Gameplay;
using HightlanderSolutions.Utilities.Pooling;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace Hightlander.Pong
{
    public sealed class BounceShockwaveItem : MonoBehaviour, IPoolItem
    {
        [SerializeField, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private ParticleSystem ParticleSystem;

        [Inject, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private Playfield Playfield;

        public async Task Show(Vector2 position)
        {
            transform.localPosition = Playfield.TranslateToWorldCoordinates(position, transform.localPosition.z);
            gameObject.SetActive(true);

            ParticleSystem.Play();

            await Task.Delay(Mathf.FloorToInt(ParticleSystem.main.duration * 1000));
        }

        #region Implementation of IPoolItem

        /// <inheritdoc />
        public void Release()
        {
            ParticleSystem.Stop();
            gameObject.SetActive(false);
        }

        #endregion
    }
}