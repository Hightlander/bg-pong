﻿using System.Threading.Tasks;
using HightlanderSolutions.Utilities.Pooling;
using JetBrains.Annotations;
using UnityEngine;

namespace Hightlander.Pong
{
    public sealed class BounceAudioSource : MonoBehaviour, IPoolItem
    {
        [SerializeField, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private AudioSource Audio;

        public async Task Play()
        {
            gameObject.SetActive(true);

            Audio.Play();
            await Task.Delay(Mathf.FloorToInt(Audio.clip.length * 1000));
        }

        #region Implementation of IPoolItem

        /// <inheritdoc />
        public void Release()
        {
            gameObject.SetActive(false);
        }

        #endregion
    }
}