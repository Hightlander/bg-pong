﻿using Hightlander.Pong.Gameplay;
using HightlanderSolutions.Utilities.Pooling;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace Hightlander.Pong
{
    public sealed class BounceAudio : MonoBehaviour
    {
        [SerializeField, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private GameObject BounceAudioSourceTemplate;

        [Inject, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private Playfield Playfield;

        private readonly PrefabInstancesPool<BounceAudioSource> Pool = new PrefabInstancesPool<BounceAudioSource>();

        private void Awake()
        {
            Pool.Initialize(BounceAudioSourceTemplate, transform, null);

            Playfield.BallSpawnedEvent += BallSpawnedEventHandler;
            Playfield.BallRemovedEvent += BallRemovedEventHandler;
        }

        private void OnDestroy()
        {
            Playfield.BallSpawnedEvent -= BallSpawnedEventHandler;
            Playfield.BallRemovedEvent -= BallRemovedEventHandler;
        }

        private void BallSpawnedEventHandler(Ball ball)
        {
            ball.BorderCollisionEvent += BallCollisionEventHandler;
            ball.PlankCollisionEvent += PlankCollisionEventHandler;
        }

        private void BallRemovedEventHandler(Ball ball)
        {
            ball.BorderCollisionEvent -= BallCollisionEventHandler;
            ball.PlankCollisionEvent -= PlankCollisionEventHandler;
        }

        private void PlankCollisionEventHandler(Ball ball, Plank plank)
        {
            Play();
        }

        private void BallCollisionEventHandler(Ball ball)
        {
            Play();
        }

        private async void Play()
        {
            BounceAudioSource NewSource = Pool.GetInstance();
            await NewSource.Play();

            if (NewSource == null) return;
            Pool.Release(NewSource);
        }
    }
}