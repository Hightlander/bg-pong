﻿using Hightlander.Pong.Gameplay;
using HightlanderSolutions.Utilities.Pooling;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace Hightlander.Pong
{
    public sealed class BounceShockwaves : MonoBehaviour
    {
        [SerializeField, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private GameObject Template;

        [Inject, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private Playfield Playfield;

        [Inject, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private IInstantiator Instantiator;

        private readonly PrefabInstancesPool<BounceShockwaveItem> Pool = new PrefabInstancesPool<BounceShockwaveItem>();

        private void Awake()
        {
            Playfield.BallSpawnedEvent += BallSpawnedEventHandler;
            Playfield.BallRemovedEvent += BallRemovedEventHandler;

            Pool.Initialize(Template, transform, Instantiator);
        }

        private void OnDestroy()
        {
            Playfield.BallSpawnedEvent -= BallSpawnedEventHandler;
            Playfield.BallRemovedEvent -= BallRemovedEventHandler;
        }

        private void BallSpawnedEventHandler(Ball ball)
        {
            ball.BorderCollisionEvent += BallCollisionEventHandler;
            ball.PlankCollisionEvent += PlankCollisionEventHandler;
        }

        private void BallRemovedEventHandler(Ball ball)
        {
            ball.BorderCollisionEvent -= BallCollisionEventHandler;
            ball.PlankCollisionEvent -= PlankCollisionEventHandler;
        }

        private void PlankCollisionEventHandler(Ball ball, Plank plank)
        {
            RunShockwave(ball.Position);
        }

        private void BallCollisionEventHandler(Ball ball)
        {
            RunShockwave(ball.Position);
        }

        private async void RunShockwave(Vector2 position)
        {
            var Shockwave = Pool.GetInstance();

            await Shockwave.Show(position);

            if (Shockwave != null) //При выходи из плеймода в редакторе все элементы уже уничтожены, а асинхронная операция продолжается.
            {
                Pool.Release(Shockwave);
            }
        }
    }
}