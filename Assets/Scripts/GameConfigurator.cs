﻿using System.Collections.Generic;
using Hightlander.Pong.Gameplay;
using Hightlander.Pong.Networking;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace Hightlander.Pong
{
    public sealed class GameConfigurator : MonoBehaviour
    {
        [SerializeField, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private bool UseAI = true;

        [Header("Playfield settings")]
        [Tooltip("Соотношение сторон игрового поля (в мультиплеере)")]
        [SerializeField, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private float PlayfieldAspectRatio = 16f / 9f;

        [Tooltip("Использовать ли соотношение сторон в одиночной игре или занимать весь доступный экран")]
        [SerializeField, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private bool UseScreenAspectInSingleplayer = true;

        [Header("Plank settings")]
        [Range(0.001f, 1f), Tooltip("Высота планки в долях от высоты поля")]
        [SerializeField, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private float PlankHeight = 0.02f;

        [Range(0.01f, 1f), Tooltip("Ширина планки в долях от ширины поля")]
        [SerializeField, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private float PlankWidth = 0.1f;

        [SerializeField, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private float PlankVerticalOffset = 0.04f;

        [SerializeField, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private Color OwnPlankColor = Color.green;

        [SerializeField, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private Color EnemyPlankColor = Color.red;


        [Inject, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private Playfield Playfield;

        [Inject, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private SessionController Session;

        [Inject, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private IInstantiator Instantiator;

        private BasicBallManager BallManager;

        private readonly List<IPlankController> PlankControllers = new List<IPlankController>();

        /// <summary>
        ///     Сконфигурировать игру для одиночной игры.
        /// </summary>
        [PublicAPI]
        public void ConfigureForSingleplayer()
        {
            DisposePlankControllers();

            Playfield.Configure(GeneratePlayfieldSettings(UseScreenAspectInSingleplayer));

            float ActualPlankWidth = PlankWidth * Playfield.Settings.AspectRatio;
            Plank BottomPlank = new Plank(0, true, ActualPlankWidth, PlankHeight, new Vector2(Playfield.Settings.AspectRatio / 2f, 1f - PlankVerticalOffset), Vector2.up);
            Plank UpperPlank = new Plank(1, true, ActualPlankWidth, PlankHeight, new Vector2(Playfield.Settings.AspectRatio / 2f, PlankVerticalOffset), Vector2.down);

            Playfield.AddPlank(BottomPlank).AddPlank(UpperPlank);

            BottomPlank.SetColor(OwnPlankColor);

            var InputPlankController = CreatePlankController<InputPlankController>();
            var AIPlankController = CreatePlankController<AIPlankController>();

            //AIPlankController.ConnectPlank(BottomPlank);
            InputPlankController.ConnectPlank(BottomPlank);

            if (UseAI)
            {
                UpperPlank.SetColor(EnemyPlankColor);
                AIPlankController.ConnectPlank(UpperPlank);
            }
            else
            {
                UpperPlank.SetColor(OwnPlankColor);
                InputPlankController.ConnectPlank(UpperPlank);
            }

            BallManager?.Dispose();
            BallManager = Instantiator.Instantiate<SingleplayerBallManager>();
        }

        /// <summary>
        ///     Сконфигурировать игру для многопользовательской игры.
        /// </summary>
        [PublicAPI]
        public void ConfigureForMultiplayer(INetworkMessenger messenger)
        {
            DisposePlankControllers();

            Playfield.Configure(GeneratePlayfieldSettings(false));

            float ActualPlankWidth = PlankWidth * Playfield.Settings.AspectRatio;
            Plank BottomPlank = new Plank(Session.IsServer ? (short)0 : (short)1, true, ActualPlankWidth, PlankHeight, new Vector2(Playfield.Settings.AspectRatio / 2f, 1f - PlankVerticalOffset), Vector2.up);
            Plank UpperPlank = new Plank(Session.IsServer ? (short)1 : (short)0, false, ActualPlankWidth, PlankHeight, new Vector2(Playfield.Settings.AspectRatio / 2f, PlankVerticalOffset), Vector2.down);

            BottomPlank.SetColor(OwnPlankColor);
            UpperPlank.SetColor(EnemyPlankColor);

            Playfield.AddPlank(BottomPlank).AddPlank(UpperPlank);

            CreatePlankController<NetworkMasterPlankController>().SetMessenger(messenger).ConnectPlank(BottomPlank);
            CreatePlankController<NetworkSlavePlankController>().SetMessenger(messenger).ConnectPlank(UpperPlank);

            BallManager?.Dispose();
            BallManager = Instantiator.Instantiate<MultiplayerBallManager>(new[] { messenger });
        }

        /// <summary>
        ///     Подготовить настройки игрового поля.
        /// </summary>
        /// <param name="useScreenAspect">Флаг показывающий будет ли использоваться соотношение сторон экрана или <see cref="PlayfieldAspectRatio"/></param>
        /// <returns>Настройки игрового поля.</returns>
        private PlayfieldSettings GeneratePlayfieldSettings(bool useScreenAspect)
        {
            float ScreenAspectRatio = Screen.width / (float)Screen.height;
            float TargetAspect = useScreenAspect ? ScreenAspectRatio : PlayfieldAspectRatio;

            float PixelScale;
            if (TargetAspect > ScreenAspectRatio)
            {
                PixelScale = TargetAspect / Screen.width;
            }
            else
            {
                PixelScale = 1f / Screen.height;
            }

            return new PlayfieldSettings
            {
                AspectRatio = TargetAspect,
                BordersSafety = new[] { true, false, true, false },
                PixelSize = PixelScale
            };
        }

        private void DisposePlankControllers()
        {
            PlankControllers.ForEach(controller => controller.Dispose());
            PlankControllers.Clear();
        }

        private TPlankController CreatePlankController<TPlankController>() where TPlankController : IPlankController
        {
            var Controller = Instantiator.Instantiate<TPlankController>();
            PlankControllers.Add(Controller);
            return Controller;
        }
    }
}