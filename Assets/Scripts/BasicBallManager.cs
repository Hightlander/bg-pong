﻿using System;
using Hightlander.Pong.Gameplay;
using Hightlander.Pong.GameSettings;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;
using Random = UnityEngine.Random;

namespace Hightlander.Pong
{
    /// <summary>
    ///     Базовый менеджер управления мячами.
    /// </summary>
    public abstract class BasicBallManager : IDisposable
    {
        [Inject, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        protected BallManagerConfiguration Configuration;

        [Inject, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        protected IBallColorSettings BallColorSettings;

        [Inject, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        protected Playfield Playfield;

        [Inject, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        protected SessionController Session;

        [Inject, UsedImplicitly]
        protected virtual void Configure() 
        {
            Session.SessionStartedEvent += SessionStartedEventHandler;
            Session.SessionEndedEvent += SessionEndedEventHandler;
        }

        /// <summary>
        ///     Обработчик события начала игровой сессиии.
        /// </summary>
        protected abstract void SessionStartedEventHandler();

        /// <summary>
        ///     Обработчик события завершения игровой сессии.
        /// </summary>
        protected abstract void SessionEndedEventHandler();

        /// <summary>
        ///     Создать и добавить на поле новый мяч.
        /// </summary>
        /// <returns>Свежесозданный мяч.</returns>
        [NotNull]
        protected Ball SpawnBall()
        {
            Ball NewBall = new Ball
            {
                Position = new Vector2(Playfield.Settings.AspectRatio / 2f, 0.5f),
                Direction = new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f)).normalized,
                Speed = Random.Range(Configuration.SpeedRandomLimit.x, Configuration.SpeedRandomLimit.y),
                Radius = Random.Range(Configuration.RadiusRandomLimit.x, Configuration.RadiusRandomLimit.y),
                Color = BallColorSettings.BallColor
            };

            SpawnBall(NewBall);

            return NewBall;
        }

        /// <summary>
        ///     Создать и добавить на поле указанный мяч.
        /// </summary>
        /// <param name="ball">Мяч, который необходимо создать на поле.</param>
        protected void SpawnBall([NotNull] Ball ball)
        {
            if (ball == null)
            {
                throw new ArgumentNullException(nameof(ball));
            }

            Playfield.AddBall(ball);
        }

        #region Implementation of IDisposable

        /// <inheritdoc />
        public virtual void Dispose()
        {
            Session.SessionStartedEvent -= SessionStartedEventHandler;
            Session.SessionEndedEvent -= SessionEndedEventHandler;
        }

        #endregion
    }
}
