﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Hightlander.Pong.Gameplay;
using Hightlander.Pong.Networking;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Networking;
using Zenject;

namespace Hightlander.Pong
{
    /// <summary>
    ///     Менеджер, управляющий появлением, удалением и синхронизацией положения мячей по сети.
    /// </summary>
    [UsedImplicitly(ImplicitUseKindFlags.InstantiatedWithFixedConstructorSignature)]
    public sealed class MultiplayerBallManager : BasicBallManager, ITickable
    {
        [Inject(Id = "network-position-epsilon"), UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private float Epsilon;

        [Inject, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private TickableManager TickableManager;

        /// <summary>
        ///     Задержка удаления мяча при выходе за границы. 
        ///     Если за это время не придёт от подключенного клиента сообщение об отбитии мяча - он будет удалён.
        /// </summary>
        private const float BallRemoveDelay = 0.5f;

        private INetworkMessenger NetworkMessenger;

        private readonly Dictionary<short, Vector2> PreviouslySentPosition = new Dictionary<short, Vector2>();
        private readonly Dictionary<short, float> PreviouslyAppliedPositionTimestamp = new Dictionary<short, float>();

        private readonly Dictionary<Ball, CancellationTokenSource> BallRemovingQueue = new Dictionary<Ball, CancellationTokenSource>();

        public MultiplayerBallManager(INetworkMessenger networkMessenger)
        {
            NetworkMessenger = networkMessenger;
        }

        protected override void Configure()
        {
            base.Configure();

            TickableManager.Add(this);

            Debug.Log($"Type: {Session.Type}, Server: {Session.IsServer}");

            NetworkMessenger.Subscribe(MessageType.BallBounce, BallBounceMessageHandler);

            if (!Session.IsServer)
            {
                NetworkMessenger.Subscribe(MessageType.BallSpawn, BallSpawnMessageHandler);
                NetworkMessenger.Subscribe(MessageType.BallRemove, BallRemoveMessageHandler);
                NetworkMessenger.Subscribe(MessageType.BallPosition, BallPositionMessageHandler);
            }
            else
            {
                Playfield.BallSpawnedEvent += BallSpawnedEventHandler;
                Playfield.BallRemovedEvent += BallRemovedEventHandler;
                Playfield.BallOutOfBoundsEvent += BallOutOfBoundEventHandler;
            }
        }

        private void SendBallPosition([NotNull] Ball ball)
        {
            if (ball == null)
            {
                throw new ArgumentNullException(nameof(ball));
            }

            NetworkMessenger.SendUnreliable(MessageType.BallPosition, new BallPositionMessageBase(ball.Id, ball.Position));
            PreviouslySentPosition[ball.Id] = ball.Position;
        }

        /// <summary>
        ///     Выполняет поиск мяча по идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор мяча.</param>
        /// <param name="ball">Найденный мяч.</param>
        /// <returns><c>true</c> - если мяч найден, иначе - <c>false</c>.</returns>
        private bool TryFindBall(short id, out Ball ball)
        {
            foreach (Ball Ball in Playfield.GetBalls())
            {
                if (Ball.Id == id)
                {
                    ball = Ball;
                    return true;
                }
            }

            ball = null;
            return false;
        }

        protected override void SessionStartedEventHandler()
        {
            if (Session.Type == SessionController.SessionTypes.Multiplayer && Session.IsServer)
            {
                if (Playfield.BallsCount <= 0)
                {
                    Ball SpawnedBall = SpawnBall();

                    NetworkMessenger.SendReliable(MessageType.BallSpawn, new BallSpawnMessageBase(SpawnedBall));
                }
            }
        }

        protected override void SessionEndedEventHandler()
        {
            if (Session.Type == SessionController.SessionTypes.Multiplayer && Session.IsServer)
            {
                foreach (Ball Ball in Playfield.GetBalls())
                {
                    Playfield.RemoveBall(Ball);
                }
            }
        }

        private void BallSpawnMessageHandler(NetworkMessage netmsg)
        {
            var BallSpawnMessage = netmsg.ReadMessage<BallSpawnMessageBase>();

            SpawnBall(BallSpawnMessage.CreateBallInverted(BallColorSettings.BallColor, Playfield.Settings.PlayfieldSize));
        }

        private void BallRemoveMessageHandler(NetworkMessage netmsg)
        {
            var BallRemoveMessage = netmsg.ReadMessage<BallRemoveMessageBase>();

            if (TryFindBall(BallRemoveMessage.Id, out Ball FoundBall))
            {
                FoundBall.Position = Playfield.Settings.PlayfieldSize - BallRemoveMessage.Position;
                Playfield.RemoveBall(FoundBall);
            }
        }

        private void BallPositionMessageHandler(NetworkMessage netmsg)
        {
            var BallPositionMessage = netmsg.ReadMessage<BallPositionMessageBase>();

            if (TryFindBall(BallPositionMessage.Id, out Ball FoundBall))
            {
                float PreviousTimestamp = float.MinValue;
                if (PreviouslyAppliedPositionTimestamp.ContainsKey(BallPositionMessage.Id))
                {
                    PreviousTimestamp = PreviouslyAppliedPositionTimestamp[BallPositionMessage.Id];
                }

                if (BallPositionMessage.Timestamp > PreviousTimestamp)
                {
                    BallPositionMessage.ApplyPositionInverted(FoundBall, Playfield.Settings.PlayfieldSize);
                    PreviouslyAppliedPositionTimestamp[BallPositionMessage.Id] = BallPositionMessage.Timestamp;
                }
            }
        }

        private void BallBounceMessageHandler(NetworkMessage netmsg)
        {
            // При получении сообщения об отскоке - проверятся необходимость обновления положения мяча (на основании меток времени отправления)
            var BallBounceMessage = netmsg.ReadMessage<BallBounceMessageBase>();
            
            if (TryFindBall(BallBounceMessage.Id, out Ball FoundBall))
            {
                float PreviousTimestamp = float.MinValue;
                if (PreviouslyAppliedPositionTimestamp.ContainsKey(BallBounceMessage.Id))
                {
                    PreviousTimestamp = PreviouslyAppliedPositionTimestamp[BallBounceMessage.Id];
                }

                bool IsNewer = BallBounceMessage.Timestamp > PreviousTimestamp;
                BallBounceMessage.ApplyToBallInverted(FoundBall, IsNewer, Playfield);

                if (IsNewer)
                {
                    PreviouslyAppliedPositionTimestamp[BallBounceMessage.Id] = BallBounceMessage.Timestamp;
                }

                if (Session.IsServer && !FoundBall.Active)
                {
                    FoundBall.Active = true;

                    if (BallRemovingQueue.ContainsKey(FoundBall))
                    {
                        BallRemovingQueue[FoundBall].Cancel();
                        BallRemovingQueue.Remove(FoundBall);
                    }
                }
            }
            else
            {
                Debug.LogError("Failed to find a ball to apply bounce");
            }
        }

        private void BallSpawnedEventHandler([NotNull] Ball ball)
        {
            if (ball == null)
            {
                throw new ArgumentNullException(nameof(ball));
            }

            PreviouslySentPosition.Add(ball.Id, ball.Position);

            if (Session.IsServer)
            {
                ball.BorderCollisionEvent += BallBorderCollisionEventHandler;
            }
        }

        private void BallRemovedEventHandler([NotNull] Ball ball)
        {
            if (ball == null)
            {
                throw new ArgumentNullException(nameof(ball));
            }

            NetworkMessenger.SendReliable(MessageType.BallRemove, new BallRemoveMessageBase(ball));
            PreviouslySentPosition.Remove(ball.Id);

            if (Session.IsServer)
            {
                ball.BorderCollisionEvent -= BallBorderCollisionEventHandler;
            }
        }

        private void BallOutOfBoundEventHandler([NotNull] Ball ball)
        {
            RemoveBall(ball, BallRemoveDelay);
        }

        private void BallBorderCollisionEventHandler([NotNull] Ball ball)
        {
            // В целом синхронизация отскоков от боковых границ не требуется на данном этапе. 
            // Наличие синхронизации приводит к необходимости городить лагокомпенсацию.
            // Отскоки от стен хорошо симулируются на самом клиенте.
            //NetworkMessenger.SendReliable(MessageType.BallBounce, new BallBounceMessageBase(ball));
        }

        private async void RemoveBall([NotNull] Ball ball, float delay)
        {
            if (ball == null)
            {
                throw new ArgumentNullException(nameof(ball));
            }

            if (BallRemovingQueue.ContainsKey(ball))
            {
                BallRemovingQueue[ball].Cancel();
            }

            CancellationTokenSource CancellationTokenSource = new CancellationTokenSource();
            BallRemovingQueue[ball] = CancellationTokenSource;

            ball.Active = false;

            await Task.Delay(Mathf.FloorToInt(delay * 1000));

            if (!CancellationTokenSource.IsCancellationRequested)
            {
                Playfield.RemoveBall(ball);
            }
        }

        private void CheckAndSendBallPosition()
        {
            foreach (Ball Ball in Playfield.GetBalls())
            {
                if (Vector2.Distance(Ball.Position, PreviouslySentPosition[Ball.Id]) > Epsilon)
                {
                    SendBallPosition(Ball);
                }
            }
        }

        #region Implementation of ITickable

        /// <inheritdoc />
        public void Tick()
        {
            if (Session.IsServer)
            {
                // В целом синхронизация не требуется на данном этапе. 
                // Наличие синхронизации приводит к необходимости городить лагокомпенсацию, а это совсем другая история.
                //CheckAndSendBallPosition();
            }
        }

        #endregion

        #region Overrides of BallManager

        /// <inheritdoc />
        public override void Dispose()
        {
            base.Dispose();

            if (Session.Type == SessionController.SessionTypes.Multiplayer && !Session.IsServer)
            {
                NetworkMessenger.Unsubscribe(MessageType.BallSpawn);
                NetworkMessenger.Unsubscribe(MessageType.BallRemove);
                NetworkMessenger.Unsubscribe(MessageType.BallPosition);
                NetworkMessenger.Unsubscribe(MessageType.BallBounce);
            }

            if (Session.Type == SessionController.SessionTypes.Multiplayer && Session.IsServer)
            {
                Playfield.BallSpawnedEvent -= BallSpawnedEventHandler;
                Playfield.BallRemovedEvent -= BallRemovedEventHandler;
            }

            NetworkMessenger = null;
        }

        #endregion
    }
}