﻿using JetBrains.Annotations;
using UnityEngine;

namespace Hightlander.Pong
{
    /// <summary>
    ///     Используемая менеджером мячей конфигурация.
    /// </summary>
    [CreateAssetMenu(fileName = "BallManager Configuration", menuName = "Pong/BallManager Configuration")]
    [UsedImplicitly(ImplicitUseKindFlags.InstantiatedNoFixedConstructorSignature)]
    public sealed class BallManagerConfiguration : ScriptableObject
    {
        /// <summary>
        ///     Границы значений скорости мяча при создании.
        /// </summary>
        [MinMax(0.1f, 5f, ShowEditRange = true)]
        [SerializeField, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        public Vector2 SpeedRandomLimit = new Vector2(1f, 3f);

        /// <summary>
        ///     Границы значения радиуса мяча при создании.
        /// </summary>
        [MinMax(0.01f, 0.1f, ShowEditRange = true)]
        [SerializeField, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        public Vector2 RadiusRandomLimit = new Vector2(0.01f, 0.02f);
    }
}