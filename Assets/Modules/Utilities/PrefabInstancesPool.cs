﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace HightlanderSolutions.Utilities.Pooling
{
    public class PrefabInstancesPool<TItem> where TItem : Component, IPoolItem
    {
        public List<TItem> UsedItems { get; } = new List<TItem>();

        private readonly Queue<TItem> UnusedQueue = new Queue<TItem>();

        private GameObject Prefab;
        private Transform Root;
        private IInstantiator Instantiator;

        /// <inheritdoc />
        public void Initialize([NotNull] GameObject prefab, [NotNull] Transform root, [CanBeNull] IInstantiator instantiator)
        {
            Prefab = prefab ?? throw new ArgumentNullException(nameof(prefab));
            Root = root ?? throw new ArgumentNullException(nameof(root));
            Instantiator = instantiator;
        }

        [PublicAPI]
        public TItem GetInstance()
        {
            TItem ResultItem;
            if (UnusedQueue.Count > 0)
            {
                ResultItem = UnusedQueue.Dequeue();
            }
            else
            {
                if (Instantiator != null)
                {
                    ResultItem = Instantiator.InstantiatePrefab(Prefab, Root).GetComponent<TItem>();
                }
                else
                {
                    ResultItem = UnityEngine.Object.Instantiate(Prefab, Root).GetComponent<TItem>();
                }
            }

            UsedItems.Add(ResultItem);
            return ResultItem;
        }

        [PublicAPI]
        public void Release([NotNull] TItem item)
        {
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            item.Release();
            UnusedQueue.Enqueue(item);
        }

        [PublicAPI]
        public void ReleaseAll()
        {
            List<TItem> UsedItemsCopy = new List<TItem>(UsedItems);
            UsedItemsCopy.ForEach(Release);
        }
    }
}