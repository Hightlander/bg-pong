﻿namespace HightlanderSolutions.Utilities.Pooling
{
    public interface IPoolItem
    {
        void Release();
    }
}