﻿using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace Hightlander.Pong.GameSettings
{
    [CreateAssetMenu(fileName = "Game Settings Installer", menuName = "Installers/Game Settings Installer")]
    [UsedImplicitly(ImplicitUseKindFlags.InstantiatedNoFixedConstructorSignature)]
    public sealed class GameSettingsInstaller : ScriptableObjectInstaller<GameSettingsInstaller>
    {
        #region Overrides of ScriptableObjectInstallerBase

        /// <inheritdoc />
        public override void InstallBindings()
        {
            Container.Bind<IBallColorSettings>().To<SettingsStorage>().AsSingle();
        }

        #endregion
    }
}