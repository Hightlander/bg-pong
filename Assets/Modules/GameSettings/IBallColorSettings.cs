﻿using UnityEngine;

namespace Hightlander.Pong.GameSettings
{
    public interface IBallColorSettings
    {
        /// <summary>
        ///     Возвращает или устанавливает пользовательский цвет мяча.
        /// </summary>
        Color BallColor { get; set; }
    }
}
