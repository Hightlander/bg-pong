﻿using JetBrains.Annotations;
using UnityEngine;

namespace Hightlander.Pong.GameSettings
{
    [UsedImplicitly(ImplicitUseKindFlags.InstantiatedNoFixedConstructorSignature)]
    public sealed class SettingsStorage : IBallColorSettings
    {
        #region Implementation of IBallColorSettings

        /// <inheritdoc />
        public Color BallColor
        {
            get
            {
                if (!Initialized)
                {
                    Initialize();
                }

                return InnerBallColor;
            }

            set
            {
                InnerBallColor = value;
                RedComponent = InnerBallColor.r;
                GreenComponent = InnerBallColor.g;
                BlueComponent = InnerBallColor.b;
            }
        }

        #endregion

        #region Color Components

        private const string RedComponentKey = "ball-color_red";
        private const string GreenComponentKey = "ball-color_green";
        private const string BlueComponentKey = "ball-color_blue";

        private float RedComponent
        {
            get { return GetColorComponent(RedComponentKey); }
            set { SetColorComponent(RedComponentKey, value); }
        }

        private float GreenComponent
        {
            get { return GetColorComponent(GreenComponentKey); }
            set { SetColorComponent(GreenComponentKey, value); }
        }

        private float BlueComponent
        {
            get { return GetColorComponent(BlueComponentKey); }
            set { SetColorComponent(BlueComponentKey, value); }
        }

        #endregion

        private Color InnerBallColor;

        private bool Initialized;

        private void Initialize()
        {
            InnerBallColor = new Color(RedComponent, GreenComponent, BlueComponent);

            Initialized = true;
        }

        private float GetColorComponent(string key, float defaultValue = 1f)
        {
            return PlayerPrefs.GetFloat(key, defaultValue);
        }

        private void SetColorComponent(string key, float value)
        {
            PlayerPrefs.SetFloat(key, value);
        }
    }
}