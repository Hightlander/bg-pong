﻿using System;
using System.Collections.Generic;
using Hightlander.Pong.Gameplay;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace Hightlander.Pong.Networking
{
    /// <summary>
    ///     Контроллер управления собственной планкой для сетевой игры.
    /// </summary>
    [UsedImplicitly(ImplicitUseKindFlags.InstantiatedNoFixedConstructorSignature)]
    public class NetworkMasterPlankController : InputPlankController
    {
        /// <summary>
        ///     При изменении положения планки на большее значение - новое положение будет отправлено по сети.
        /// </summary>
        [Inject(Id = "network-position-epsilon"), UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private float PositionEpsilon = 0.0001f;

        private INetworkMessenger Messenger;

        /// <summary>
        ///     Коллекция последних отправленных положений связанных планок.
        /// </summary>
        private readonly Dictionary<Plank, float> PreviouslySentPosition = new Dictionary<Plank, float>();

        /// <summary>
        ///     Установить используемый диспетчер сетевых сообщений для сетевых сообщений.
        /// </summary>
        /// <param name="messenger">Диспетчер сетевых сообщений</param>
        [NotNull]
        public NetworkMasterPlankController SetMessenger(INetworkMessenger messenger)
        {
            Messenger = messenger;

            return this;
        }

        #region Overrides of PlankController

        /// <inheritdoc />
        [NotNull]
        public override IPlankController ConnectPlank(Plank plank)
        {
            base.ConnectPlank(plank);

            plank.BallCollisionEvent += BallCollisionEventHandler;

            return this;
        }

        /// <inheritdoc />
        protected override void PlankRemovedEventHandler(Plank plank)
        {
            base.PlankRemovedEventHandler(plank);

            PreviouslySentPosition.Remove(plank);
            plank.BallCollisionEvent -= BallCollisionEventHandler;
        }

        #endregion

        #region Overrides of InputPlankController

        /// <inheritdoc />
        public override void Tick()
        {
            base.Tick();

            foreach (Plank Plank in ConnectedPlanks)
            {
                if (!PreviouslySentPosition.ContainsKey(Plank) ||
                    (Mathf.Abs(Plank.Position.x - PreviouslySentPosition[Plank]) >= PositionEpsilon))
                {
                    PreviouslySentPosition[Plank] = Plank.Position.x;

                    Messenger.SendUnreliable(MessageType.PlankPosition, new PlankPositionMessageBase(Plank.Id, Plank.Position.x));
                }
            }
        }

        #endregion

        private void BallCollisionEventHandler(Plank plank, [NotNull] Ball ball)
        {
            if (ball == null)
            {
                throw new ArgumentNullException(nameof(ball));
            }
            
            Messenger.SendReliable(MessageType.BallBounce, new BallBounceMessageBase(ball, plank));
        }
    }
}