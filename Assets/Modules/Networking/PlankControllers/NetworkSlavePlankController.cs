﻿using System.Collections.Generic;
using Hightlander.Pong.Gameplay;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Networking;

namespace Hightlander.Pong.Networking
{
    /// <summary>
    ///     Контроллер управления планкой соперника для сетевой игры.
    /// </summary>
    [UsedImplicitly(ImplicitUseKindFlags.InstantiatedNoFixedConstructorSignature)]
    public class NetworkSlavePlankController : PlankController
    {
        private INetworkMessenger Messenger;

        /// <summary>
        ///     Коллекция полученных по сети положений связанных планок.
        /// </summary>
        private readonly Dictionary<short, float> TargetPositions = new Dictionary<short, float>();

        /// <summary>
        ///     Установить используемый диспетчер сетевых сообщений для сетевых сообщений.
        /// </summary>
        /// <param name="messenger">Диспетчер сетевых сообщений</param>
        [NotNull]
        public NetworkSlavePlankController SetMessenger(INetworkMessenger messenger)
        {
            Messenger = messenger;
            Messenger.Subscribe(MessageType.PlankPosition, HandlePlankPositionMessage);

            return this;
        }

        private void HandlePlankPositionMessage(NetworkMessage netmsg)
        {
            var Message = netmsg.ReadMessage<PlankPositionMessageBase>();

            // Инвертируем координаты планки, т.к. относительно хоста наше игровое поле повёрнуто.
            TargetPositions[Message.PlankId] = Playfield.Settings.AspectRatio - Message.Position;
        }

        #region Overrides of PlankController

        /// <inheritdoc />
        public override void Tick()
        {
            foreach (Plank Plank in ConnectedPlanks)
            {
                if (TargetPositions.ContainsKey(Plank.Id))
                {
                    Plank.Position = new Vector2(Mathf.Lerp(Plank.Position.x, TargetPositions[Plank.Id], 0.3f), Plank.Position.y);
                    //Plank.Position = new Vector2(TargetPositions[Plank.Id], Plank.Position.y);
                }
            }
        }

        #endregion
    }
}