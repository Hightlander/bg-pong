﻿using System;
using System.Threading;
using System.Threading.Tasks;
using JetBrains.Annotations;

namespace Hightlander.Pong.Networking
{
    public interface IPongServer : INetworkMessenger, IDisposable
    {
        /// <summary>
        ///     Запустить сервер.
        /// </summary>
        /// <param name="port">Порт прослушивания подключений.</param>
        [NotNull]
        IPongServer Run(int port = PongServer.DefaultPort);

        /// <summary>
        ///     Остановить сервер.
        /// </summary>
        void Stop();

        /// <summary>
        ///     Асинхронная операция ожидания подключения другого игрока.
        /// </summary>
        /// <param name="cancellationToken">Токен отмены ожидания.</param>
        /// <returns>Флаг успешности подключения другого игрока.</returns>
        [NotNull]
        Task<bool> WaitForAnotherPlayer(CancellationToken cancellationToken);

        /// <summary>
        ///     Второй игрок был отключен.
        /// </summary>
        event Action PlayerDisconnectedEvent;
    }
}