﻿using System;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

namespace Hightlander.Pong.Networking
{
    public sealed class MockPongServer : IPongServer
    {
        #region Implementation of IPongServer

        /// <inheritdoc />
        public IPongServer Run(int port = PongServer.DefaultPort)
        {
            return this;
        }

        /// <inheritdoc />
        public void Stop()
        {

        }

        /// <inheritdoc />
        public async Task<bool> WaitForAnotherPlayer(CancellationToken cancellationToken)
        {
            await Task.Delay(2 * 1000, cancellationToken);
            return !cancellationToken.IsCancellationRequested;
        }

        /// <inheritdoc />
        public event Action PlayerDisconnectedEvent;

        #endregion

        #region Implementation of INetworkMessenger

        /// <inheritdoc />
        public void SendUnreliable<TMessageBase>(MessageType messageType, TMessageBase message) where TMessageBase : MessageBase
        {
        }

        /// <inheritdoc />
        public void SendReliable<TMessageBase>(MessageType messageType, TMessageBase message) where TMessageBase : MessageBase
        {
        }

        /// <inheritdoc />
        public void Subscribe(MessageType messageType, NetworkMessageDelegate callback)
        {
            Debug.Log($"Subscribed on {messageType} network message");
        }

        /// <inheritdoc />
        public void Unsubscribe(MessageType messageType)
        {
        }

        #endregion

        #region Implementation of IDisposable

        /// <inheritdoc />
        public void Dispose()
        {
        }

        #endregion
    }
}