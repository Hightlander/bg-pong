﻿using System;
using System.Threading;
using System.Threading.Tasks;
using UnityAsync;
using UnityEngine;
using UnityEngine.Networking;

namespace Hightlander.Pong.Networking
{
    /// <summary>
    ///     Сервер сетевой игры.
    /// </summary>
    public sealed class PongServer : IPongServer
    {
        /// <summary>
        ///     Используемый по умолчанию порт.
        /// </summary>
        public const int DefaultPort = 4896;

        /// <summary>
        ///     Используемый порт.
        /// </summary>
        public int Port { get; private set; }

        /// <summary>
        ///     Флаг активности сервера.
        /// </summary>
        public bool Active { get; private set; }

        /// <summary>
        ///     Флаг подключенности другого игрока к серверу.
        /// </summary>
        public bool PlayerConnected { get; private set; }

        public event Action PlayerDisconnectedEvent;

        /// <inheritdoc />
        public IPongServer Run(int port = DefaultPort)
        {
            Port = port;

            Active = NetworkServer.Listen(port);
            NetworkServer.RegisterHandler(MsgType.Connect, ConnectHandler);
            NetworkServer.RegisterHandler(MsgType.Disconnect, DisconnectHandler);

            return this;
        }

        /// <inheritdoc />
        public void Stop()
        {
            if (NetworkServer.active)
            {
                NetworkServer.Shutdown();
                NetworkServer.UnregisterHandler(MsgType.Connect);
                NetworkServer.UnregisterHandler(MsgType.Disconnect);
            }
        }

        /// <inheritdoc />
        public async Task<bool> WaitForAnotherPlayer(CancellationToken cancellationToken)
        {
            await Await.Until(() => PlayerConnected || cancellationToken.IsCancellationRequested);

            return PlayerConnected && !cancellationToken.IsCancellationRequested;
        }

        /// <inheritdoc />
        public void SendUnreliable<TMessageBase>(MessageType messageType, TMessageBase message) where TMessageBase : MessageBase
        {
            Send(messageType, message, Channels.DefaultUnreliable);
        }

        /// <inheritdoc />
        public void SendReliable<TMessageBase>(MessageType messageType, TMessageBase message) where TMessageBase : MessageBase
        {
            Send(messageType, message, Channels.DefaultReliable);
        }

        private void Send<TMessageBase>(MessageType messageType, TMessageBase message, int channelId) where TMessageBase : MessageBase
        {
            if (!NetworkServer.active || NetworkServer.connections.Count <= 1 || NetworkServer.connections[1] == null)
            {
                return;
            }

            NetworkServer.connections[1].SendByChannel((short)messageType, message, channelId);
        }

        /// <inheritdoc />
        public void Subscribe(MessageType messageType, NetworkMessageDelegate callback)
        {
            NetworkServer.RegisterHandler((short)messageType, callback);
        }

        /// <inheritdoc />
        public void Unsubscribe(MessageType messageType)
        {
            NetworkServer.UnregisterHandler((short)messageType);
        }

        private void ConnectHandler(NetworkMessage message)
        {
            PlayerConnected = true;
        }

        private void DisconnectHandler(NetworkMessage netmsg)
        {
            PlayerDisconnectedEvent?.Invoke();
        }

        #region Implementation of IDisposable

        /// <inheritdoc />
        public void Dispose()
        {
            Stop();
        }

        #endregion
    }
}