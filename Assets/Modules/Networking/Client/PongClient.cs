﻿using System;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

namespace Hightlander.Pong.Networking
{
    /// <summary>
    ///     Клиент сетевой игры.
    /// </summary>
    public sealed class PongClient : IPongClient
    {
        private NetworkClient Client;

        /// <summary>
        ///     Флаг подключенности к хосту.
        /// </summary>
        public bool Connected { get; private set; }

        /// <summary>
        ///     Подключение было потеряно.
        /// </summary>
        public event Action ConnectionLostEvent;

        private TaskCompletionSource<bool> ConnectionTaskCompletionSource;


        /// <inheritdoc />
        public async Task<bool> Connect(string ip, int port = PongServer.DefaultPort)
        {
            if (ip == null)
            {
                throw new ArgumentNullException(nameof(ip));
            }

            Client = new NetworkClient();

            string Ip = ip.Trim('​');

            ConnectionTaskCompletionSource = new TaskCompletionSource<bool>();
            Client.Connect(Ip, port);

            Client.RegisterHandler(MsgType.Connect, ConnectedHandler);
            Client.RegisterHandler(MsgType.Disconnect, DisconnectedHandler);
            Client.RegisterHandler(MsgType.Error, ErrorHandler);

            return await ConnectionTaskCompletionSource.Task;
        }

        private void DisconnectedHandler(NetworkMessage netmsg)
        {
            Connected = false;
            ConnectionLostEvent?.Invoke();
        }

        private void ConnectedHandler(NetworkMessage netmsg)
        {
            Connected = true;
            ConnectionTaskCompletionSource?.SetResult(true);
        }

        private void ErrorHandler(NetworkMessage netmsg)
        {
            ErrorMessage Message = netmsg.ReadMessage<ErrorMessage>();
            Debug.LogError($"Error connecting with code {Message.errorCode}");
            ConnectionTaskCompletionSource?.SetResult(false);
        }

        #region Implementation of INetworkMessenger

        /// <inheritdoc />
        public void SendUnreliable<TMessageBase>(MessageType messageType, TMessageBase message) where TMessageBase : MessageBase
        {
            Client.SendUnreliable((short)messageType, message);
        }

        /// <inheritdoc />
        public void SendReliable<TMessageBase>(MessageType messageType, TMessageBase message) where TMessageBase : MessageBase
        {
            Client.Send((short)messageType, message);
        }

        /// <inheritdoc />
        public void Subscribe(MessageType messageType, NetworkMessageDelegate callback)
        {
            Client.RegisterHandler((short)messageType, callback);
        }

        /// <inheritdoc />
        public void Unsubscribe(MessageType messageType)
        {
            Client.UnregisterHandler((short)messageType);
        }

        #endregion

        #region Implementation of IDisposable

        /// <inheritdoc />
        public void Dispose()
        {
            if (Connected)
            {
                Client?.Disconnect();
            }
        }

        #endregion
    }
}