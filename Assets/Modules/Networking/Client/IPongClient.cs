﻿using System;
using System.Threading.Tasks;
using JetBrains.Annotations;

namespace Hightlander.Pong.Networking
{
    public interface IPongClient : INetworkMessenger, IDisposable
    {
        /// <summary>
        ///     Подключен ли клиент к серверу.
        /// </summary>
        bool Connected { get; }

        /// <summary>
        ///     Выполнить попытку подключения к серверу.
        /// </summary>
        /// <param name="ip">Адрес сервера.</param>
        /// <param name="port">Порт подключения.</param>
        /// <returns>Флаг успешности выполнения подключения.</returns>
        [NotNull]
        Task<bool> Connect([NotNull] string ip, int port = PongServer.DefaultPort);

        /// <summary>
        ///     Подключение было потеряно.
        /// </summary>
        event Action ConnectionLostEvent;
    }
}