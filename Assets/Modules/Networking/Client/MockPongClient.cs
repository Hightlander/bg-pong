﻿using System;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

namespace Hightlander.Pong.Networking
{
    public sealed class MockPongClient : IPongClient
    {
        #region Implementation of IPongClient

        /// <inheritdoc />
        public bool Connected { get; private set; }

        /// <inheritdoc />
        public async Task<bool> Connect(string ip, int port = PongServer.DefaultPort)
        {
            Connected = true;

            await Task.Delay(1000);

            return true;
        }

        /// <inheritdoc />
        public event Action ConnectionLostEvent;

        #endregion

        #region Implementation of INetworkMessenger

        /// <inheritdoc />
        public void SendUnreliable<TMessageBase>(MessageType messageType, TMessageBase message) where TMessageBase : MessageBase
        {
        }

        /// <inheritdoc />
        public void SendReliable<TMessageBase>(MessageType messageType, TMessageBase message) where TMessageBase : MessageBase
        {
        }

        /// <inheritdoc />
        public void Subscribe(MessageType messageType, NetworkMessageDelegate callback)
        {
            Debug.Log($"Subscribed on {messageType} network message");
        }

        /// <inheritdoc />
        public void Unsubscribe(MessageType messageType)
        {
            Debug.Log($"Subscribed from {messageType} network message");
        }

        #endregion

        #region Implementation of IDisposable

        /// <inheritdoc />
        public void Dispose()
        {
        }

        #endregion
    }
}