﻿using UnityEngine.Networking;

namespace Hightlander.Pong.Networking
{
    public enum MessageType
    {
        /// <summary>
        ///     Событие обновления положения планки.
        /// </summary>
        PlankPosition = MsgType.Highest + 1,

        /// <summary>
        ///     Событие обновления положения мяча.
        /// </summary>
        BallPosition = MsgType.Highest + 2,

        /// <summary>
        ///     Событие создания нового мяча.
        /// </summary>
        BallSpawn = MsgType.Highest + 3,

        /// <summary>
        ///     Событие удаления мяча.
        /// </summary>
        BallRemove = MsgType.Highest + 4,

        /// <summary>
        ///     Событие отскока мяча от поверхности.
        /// </summary>
        BallBounce = MsgType.Highest + 5
    }
}