﻿using System;
using Hightlander.Pong.Gameplay;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Networking;

namespace Hightlander.Pong.Networking
{
    /// <summary>
    ///     Сетевое сообщение с информацией об актуальном положении мяча.
    /// </summary>
    public sealed class BallPositionMessageBase : MessageBase
    {
        /// <summary>
        ///     Временная метка сообщения.
        /// </summary>
        public float Timestamp { get; private set; }

        /// <summary>
        ///     Идентификатор мяча.
        /// </summary>
        public short Id { get; private set; }

        /// <summary>
        ///     Положение мяча после отскока.
        /// </summary>
        public Vector2 RawPosition { get; private set; }

        public BallPositionMessageBase()
        {

        }

        /// <inheritdoc />
        public BallPositionMessageBase(short id, Vector2 position)
        {
            Timestamp = Time.time;
            Id = id;
            RawPosition = position;
        }

        /// <summary>
        ///     Получить перевёрнутое положение мяча. (Поля у клиента и сервера повернуты по разному)
        /// </summary>
        /// <param name="playfieldSize">Размеры игрового поля.</param>
        /// <returns>Перевёрнутое положение мяча.</returns>
        public Vector2 GetPositionInverted(Vector2 playfieldSize)
        {
            return playfieldSize - RawPosition;
        }

        /// <summary>
        ///     Применить перевёрнутое положение мяча к целевому мячу. (Поля у клиента и сервера повернуты по разному)
        /// </summary>
        /// <param name="ball">Целевой мяч.</param>
        /// <param name="playfieldSize">Размеры игрового поля.</param>
        public void ApplyPositionInverted([NotNull] Ball ball, Vector2 playfieldSize)
        {
            if (ball == null)
            {
                throw new ArgumentNullException(nameof(ball));
            }

            Vector2 TargetPosition = GetPositionInverted(playfieldSize);
            ball.Position = Vector2.Lerp(ball.Position, TargetPosition, 0.3f);
        }


        #region Overrides of MessageBase

        /// <inheritdoc />
        public override void Serialize(NetworkWriter writer)
        {
            writer.Write(Timestamp);
            writer.Write(Id);
            writer.Write(RawPosition);

            base.Serialize(writer);
        }

        /// <inheritdoc />
        public override void Deserialize(NetworkReader reader)
        {
            Timestamp = reader.ReadSingle();
            Id = reader.ReadInt16();
            RawPosition = reader.ReadVector2();

            base.Deserialize(reader);
        }

        #endregion
    }
}