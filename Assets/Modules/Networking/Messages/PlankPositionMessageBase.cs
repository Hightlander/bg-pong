﻿using UnityEngine.Networking;

namespace Hightlander.Pong.Networking
{
    /// <summary>
    ///     Сетевое сообщение с информацией о положении планки.
    /// </summary>
    public sealed class PlankPositionMessageBase : MessageBase
    {
        /// <summary>
        ///     Идентификатор планки.
        /// </summary>
        public short PlankId { get; private set; }
        
        /// <summary>
        ///     Новое положение планки по оси движения.
        /// </summary>
        public float Position { get; private set; }

        public PlankPositionMessageBase()
        {
            
        }

        /// <inheritdoc />
        public PlankPositionMessageBase(short plankId, float position)
        {
            PlankId = plankId;
            Position = position;
        }


        #region Overrides of MessageBase

        /// <inheritdoc />
        public override void Serialize(NetworkWriter writer)
        {
            writer.Write(PlankId);
            writer.Write(Position);

            base.Serialize(writer);
        }

        /// <inheritdoc />
        public override void Deserialize(NetworkReader reader)
        {
            PlankId = reader.ReadInt16();
            Position = reader.ReadSingle();

            base.Deserialize(reader);
        }

        #endregion
    }
}