﻿using System;
using System.Linq;
using Hightlander.Pong.Gameplay;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Networking;

namespace Hightlander.Pong.Networking
{
    /// <summary>
    ///     Сетевое сообщение с информацией о событии отскока мяча.
    /// </summary>
    public sealed class BallBounceMessageBase : MessageBase
    {
        //TODO: Можно оптимизировать пересылаемое количество данных, если скорость зашивать в вектор направления.

        /// <summary>
        ///     Временная метка сообщения.
        /// </summary>
        public float Timestamp { get; private set; }

        /// <summary>
        ///     Идентификатор мяча.
        /// </summary>
        public short Id { get; private set; }

        /// <summary>
        ///     Идентификатор планки.
        /// </summary>
        public short PlankId { get; private set; }

        /// <summary>
        ///     Положение мяча после отскока.
        /// </summary>
        public Vector2 Position { get; private set; }

        /// <summary>
        ///     Направление движения мяча после отскока.
        /// </summary>
        public Vector2 Direction { get; private set; }

        /// <summary>
        ///     Скорость движения мяча после отскока.
        /// </summary>
        public float Speed { get; private set; }

        public BallBounceMessageBase()
        {

        }

        public BallBounceMessageBase([NotNull] Ball ball, [NotNull] Plank plank)
        {
            if (ball == null)
            {
                throw new ArgumentNullException(nameof(ball));
            }

            Timestamp = Time.time;

            Id = ball.Id;
            PlankId = plank.Id;
            Position = ball.Position;
            Direction = ball.Direction;
            Speed = ball.Speed;
        }

        /// <summary>
        ///     Применить значения мяча после отскока к указанному мячу.
        /// </summary>
        /// <param name="ball">Мяч к которому необходимо применить значения.</param>
        /// <param name="updatePosition">Флаг показывающий необходимость применения нового положения. Можно не обновлять, если сообщение пришло позднее схожих событий.</param>
        /// <param name="playfield">Игровое поле.</param>
        public void ApplyToBallInverted([NotNull] Ball ball, bool updatePosition, [NotNull] Playfield playfield)
        {
            if (ball == null)
            {
                throw new ArgumentNullException(nameof(ball));
            }

            if (updatePosition)
            {
                Vector2 TargetPosition = playfield.Settings.PlayfieldSize - Position;
                ball.Position = Vector2.Lerp(ball.Position, TargetPosition, 0.5f);
            }
            ball.Direction = -Direction;
            ball.Speed = Speed;

            Plank CollidedPlank = playfield.GetPlanks().FirstOrDefault(plank => plank.Id == PlankId);
            if (CollidedPlank != null)
            {
                ball.RaisePlankCollisionEvent(CollidedPlank);
                CollidedPlank.RaiseBallCollisionEvent(ball);
            }
            else
            {
                Debug.LogError("Failed to find collided plank.");
            }
        }

        #region Overrides of MessageBase

        /// <inheritdoc />
        public override void Serialize(NetworkWriter writer)
        {
            writer.Write(Timestamp);
            writer.Write(Id);
            writer.Write(PlankId);
            writer.Write(Position);
            writer.Write(Direction);
            writer.Write(Speed);

            base.Serialize(writer);
        }

        /// <inheritdoc />
        public override void Deserialize(NetworkReader reader)
        {
            Timestamp = reader.ReadSingle();
            Id = reader.ReadInt16();
            PlankId = reader.ReadInt16();
            Position = reader.ReadVector2();
            Direction = reader.ReadVector2();
            Speed = reader.ReadSingle();

            base.Deserialize(reader);
        }

        #endregion
    }
}