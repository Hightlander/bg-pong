﻿using System;
using Hightlander.Pong.Gameplay;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Networking;

namespace Hightlander.Pong.Networking
{
    public sealed class BallSpawnMessageBase : MessageBase
    {
        //TODO: Можно оптимизировать пересылаемое количество данных, если скорость зашивать в вектор направления.

        /// <summary>
        ///     Идентификатор мяча.
        /// </summary>
        public short Id { get; private set; }

        /// <summary>
        ///     Положение мяча после отскока.
        /// </summary>
        public Vector2 Position { get; private set; }

        /// <summary>
        ///     Направление движения мяча после отскока.
        /// </summary>
        public Vector2 Direction { get; private set; }

        /// <summary>
        ///     Скорость движения мяча после отскока.
        /// </summary>
        public float Speed { get; private set; }

        /// <summary>
        ///     Радиус мяча.
        /// </summary>
        public float Radius { get; private set; }

        public BallSpawnMessageBase()
        {

        }

        public BallSpawnMessageBase([NotNull] Ball ball)
        {
            if (ball == null)
            {
                throw new ArgumentNullException(nameof(ball));
            }

            Id = ball.Id;
            Position = ball.Position;
            Direction = ball.Direction;
            Speed = ball.Speed;
            Radius = ball.Radius;
        }

        /// <summary>
        ///     Создать новый мяч из переданных по сети данных.
        /// </summary>
        /// <param name="color">Цвет мяча.</param>
        /// <returns>Новый мяч с характеристиками переданными в сетевом сообщении.</returns>
        [NotNull]
        public Ball CreateBall(Color color)
        {
            return new Ball(Id)
            {
                Position = Position,
                Direction = Direction,
                Speed = Speed,
                Radius = Radius,
                Color = color
            };
        }

        /// <summary>
        ///     Создать новый мяч из переданных по сети данных.
        ///     При этом инвертируются вектора, т.к. относительно хоста в мультиплеере наше игровое поле повёрнуто.
        /// </summary>
        /// <param name="color">Цвет мяча.</param>
        /// <param name="playfieldSize">Относительный размер игрового поля</param>
        /// <returns>Новый мяч с характеристиками переданными в сетевом сообщении.</returns>
        [NotNull]
        public Ball CreateBallInverted(Color color, Vector2 playfieldSize)
        {
            Ball Ball = CreateBall(color);

            Ball.Position = playfieldSize - Ball.Position;
            Ball.Direction = -Ball.Direction;

            return Ball;
        }

        #region Overrides of MessageBase

        /// <inheritdoc />
        public override void Serialize(NetworkWriter writer)
        {
            writer.Write(Id);
            writer.Write(Position);
            writer.Write(Direction);
            writer.Write(Speed);
            writer.Write(Radius);

            base.Serialize(writer);
        }

        /// <inheritdoc />
        public override void Deserialize(NetworkReader reader)
        {
            Id = reader.ReadInt16();
            Position = reader.ReadVector2();
            Direction = reader.ReadVector2();
            Speed = reader.ReadSingle();
            Radius = reader.ReadSingle();

            base.Deserialize(reader);
        }

        #endregion
    }
}