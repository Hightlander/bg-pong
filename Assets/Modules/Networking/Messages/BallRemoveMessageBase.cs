﻿using System;
using Hightlander.Pong.Gameplay;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Networking;

namespace Hightlander.Pong.Networking
{
    public sealed class BallRemoveMessageBase : MessageBase
    {
        /// <summary>
        ///     Идентификатор мяча.
        /// </summary>
        public short Id { get; private set; }

        /// <summary>
        ///     Положение мяча после отскока.
        /// </summary>
        public Vector2 Position { get; private set; }

        public BallRemoveMessageBase()
        {

        }

        public BallRemoveMessageBase([NotNull] Ball ball)
        {
            if (ball == null)
            {
                throw new ArgumentNullException(nameof(ball));
            }

            Id = ball.Id;
            Position = ball.Position;
        }

        #region Overrides of MessageBase

        /// <inheritdoc />
        public override void Serialize(NetworkWriter writer)
        {
            writer.Write(Id);
            writer.Write(Position);

            base.Serialize(writer);
        }

        /// <inheritdoc />
        public override void Deserialize(NetworkReader reader)
        {
            Id = reader.ReadInt16();
            Position = reader.ReadVector2();

            base.Deserialize(reader);
        }

        #endregion
    }
}