﻿using UnityEngine.Networking;

namespace Hightlander.Pong.Networking
{
    public interface INetworkMessenger
    {
        void SendUnreliable<TMessageBase>(MessageType messageType, TMessageBase message)
            where TMessageBase : MessageBase;

        void SendReliable<TMessageBase>(MessageType messageType, TMessageBase message)
            where TMessageBase : MessageBase;

        void Subscribe(MessageType messageType, NetworkMessageDelegate callback);
        void Unsubscribe(MessageType messageType);
    }
}