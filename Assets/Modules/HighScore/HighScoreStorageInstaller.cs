﻿using UnityEngine;
using Zenject;

namespace Hightlander.Pong.HighScore
{
    [CreateAssetMenu(fileName = "HighScoreStorageInstaller", menuName = "Installers/HighScore Storage Installer")]
    public class HighScoreStorageInstaller : ScriptableObjectInstaller<HighScoreStorageInstaller>
    {
        #region Overrides of ScriptableObjectInstallerBase

        /// <inheritdoc />
        public override void InstallBindings()
        {
            Container.Bind<IHighScoreStorage>().To<PrefsHighScoreStorage>().FromNew().AsTransient();
        }

        #endregion
    }
}