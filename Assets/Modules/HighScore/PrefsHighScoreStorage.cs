﻿using UnityEngine;

namespace Hightlander.Pong.HighScore
{
    public class PrefsHighScoreStorage : IHighScoreStorage
    {
        private const string HighScoreKey = "high-score";

        #region Implementation of IHighScoreStorage

        /// <inheritdoc />
        public int GetHighScore()
        {
            return Mathf.Clamp(PlayerPrefs.GetInt(HighScoreKey, 0), 0, int.MaxValue);
        }

        /// <inheritdoc />
        public void SetHighScore(int score)
        {
            PlayerPrefs.SetInt(HighScoreKey, Mathf.Clamp(score, 0, int.MaxValue));
            PlayerPrefs.Save();
        }

        #endregion
    }
}