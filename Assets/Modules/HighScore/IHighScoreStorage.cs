﻿namespace Hightlander.Pong.HighScore
{
    public interface IHighScoreStorage
    {
        /// <summary>
        ///     Gets stored high score.
        /// </summary>
        int GetHighScore();

        /// <summary>
        ///     Saves the high score.
        /// </summary>
        /// <param name="score">The score to save.</param>
        void SetHighScore(int score);
    }
}
