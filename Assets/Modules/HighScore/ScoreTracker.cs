﻿using Hightlander.Pong.Gameplay;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace Hightlander.Pong.HighScore
{
    /// <summary>
    ///     Трекер игрового счёта.
    /// </summary>
    public sealed class ScoreTracker : MonoBehaviour
    {
        [Inject, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private SessionController Session;

        [Inject, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private Playfield Playfield;

        [Inject, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private IHighScoreStorage HighScoreStorage;
        
        /// <summary>
        ///     Текущий счёт в сессии.
        /// </summary>
        public int CurrentScore { get; private set; }

        /// <summary>
        ///     Лучший счёт.
        /// </summary>
        public int HighScore { get; private set; }

        [Inject, UsedImplicitly]
        private void Configure()
        {
            Session.SessionStartedEvent += SessionStartedEventHandler;
            Session.SessionEndedEvent += SessionEndedEventHandler;
            Playfield.BallSpawnedEvent += BallSpawnedEventHandler;
            Playfield.BallRemovedEvent += BallRemovedEventHandler;

            HighScore = HighScoreStorage.GetHighScore();
        }

        private void OnDestroy()
        {
            Session.SessionStartedEvent -= SessionStartedEventHandler;
            Session.SessionEndedEvent -= SessionEndedEventHandler;
            Playfield.BallSpawnedEvent -= BallSpawnedEventHandler;
            Playfield.BallRemovedEvent -= BallRemovedEventHandler;
        }

        private void SessionStartedEventHandler()
        {
            CurrentScore = 0;
        }

        private void SessionEndedEventHandler()
        {
            if (CurrentScore > HighScore)
            {
                HighScore = CurrentScore;
                HighScoreStorage.SetHighScore(CurrentScore);
            }
        }

        private void BallSpawnedEventHandler(Ball ball)
        {
            ball.PlankCollisionEvent += BallCollidedWithPlank;
        }

        private void BallRemovedEventHandler(Ball ball)
        {
            ball.PlankCollisionEvent -= BallCollidedWithPlank;
        }

        private void BallCollidedWithPlank(Ball ball, Plank plank)
        {
            CurrentScore++;
        }
    }
}