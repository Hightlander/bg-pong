﻿using UnityEngine;

namespace Hightlander.Pong.Gameplay.DebugUtilities
{
    public static class RelativeDebugDrawer
    {
        public static void DrawLine(Vector2 start, Vector2 end, Color color, float duration, PlayfieldSettings settings)
        {
            Debug.DrawLine(TranslateToWorld(start, settings), TranslateToWorld(end, settings), color, duration, false);

        }

        public static Vector3 TranslateToWorld(Vector2 position, PlayfieldSettings settings)
        {
            float WidthDelta = Screen.width - settings.AspectRatio / settings.PixelSize;
            float HeightDelta = Screen.height - 1f / settings.PixelSize;

            position = new Vector2(position.x / settings.PixelSize + WidthDelta / 2f, Screen.height - position.y / settings.PixelSize - HeightDelta / 2f);

            Vector3 WorldPosition = Camera.main.ScreenToWorldPoint(position);
            return new Vector3(WorldPosition.x, WorldPosition.y, 1f);
        }
    }
}