﻿using UnityEngine;

namespace Hightlander.Pong.Gameplay
{
    public struct PlayfieldSettings
    {
        /// <summary>
        ///     <para>
        ///         Соотношение сторон игрового поля. 
        ///     </para>
        ///     <para>
        ///         Все значения и координаты хранятся в относительных единицах. При этом высота поля всегда 1, а ширина совпадает с AspectRatio.
        ///     </para>
        /// </summary>
        public float AspectRatio;

        /// <summary>
        ///     Размер пикселя на игровом поле в относительных единицах.
        ///     Помогает переводить из относительных в экранные координаты.
        /// </summary>
        public float PixelSize;

        /// <summary>
        ///     <para>
        ///         Безопасность границ (безопасная граница - граница от которой отскакивает шар не приводя к поражению)
        ///     </para>
        ///     <para>
        ///         0 - левая, 1 - верхняя, 2 - правая, 3 - нижняя.
        ///     </para>
        /// </summary>
        public bool[] BordersSafety;

        public Vector2 PlayfieldSize
        {
            get { return new Vector2(AspectRatio, 1f); }
        }
    }
}