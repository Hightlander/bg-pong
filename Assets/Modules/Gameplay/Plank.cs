﻿using System;
using JetBrains.Annotations;
using UnityEngine;

namespace Hightlander.Pong.Gameplay
{
    /// <summary>
    ///     Планка для отбивания мяча.
    /// </summary>
    public class Plank
    {
        public delegate void PlankBallCollisionEventDelegate(Plank plank, Ball ball);

        /// <summary>
        ///     Идентификатор планки.
        /// </summary>
        public short Id { get; private set; }

        /// <summary>
        ///     Флаг владения планкой.
        /// </summary>
        public bool Owned { get; private set; }

        /// <summary>
        ///     Ширина планки.
        /// </summary>
        public float Width { get; set; }

        /// <summary>
        ///     Высота планки.
        /// </summary>
        public float Height { get; set; }

        /// <summary>
        ///     Положение планки.
        /// </summary>
        public Vector2 Position { get; set; }

        /// <summary>
        ///     Нормаль отражающей поверхности планки.
        /// </summary>
        public Vector2 Normal { get; set; }

        /// <summary>
        ///     Цвет планки.
        /// </summary>
        public Color Color { get; private set; }

        /// <summary>
        ///     Событие отскока мяча от планки.
        /// </summary>
        public event PlankBallCollisionEventDelegate BallCollisionEvent;

        /// <summary>
        ///     Событие изменения цвета планки.
        /// </summary>
        public event Action<Color> ColorChangedEvent;

        public Plank(short id, bool owned, float width, float height, Vector2 position, Vector2 normal)
        {
            Id = id;
            Owned = owned;
            Width = width;
            Height = height;
            Position = position;
            Normal = normal;
        }

        /// <summary>
        ///     Установить новый цвет планки.
        /// </summary>
        /// <param name="color">Цвет планки.</param>
        [NotNull]
        public Plank SetColor(Color color)
        {
            Color = color;
            ColorChangedEvent?.Invoke(color);

            return this;
        }

        /// <summary>
        ///     Получить прямоугольник положения планки.
        /// </summary>
        public Rect GetRectangle()
        {
            return new Rect(Position.x - Width/2f, Position.y - Height/2f, Width, Height);
        }

        public void RaiseBallCollisionEvent(Ball ball)
        {
            BallCollisionEvent?.Invoke(this, ball);
        }
    }
}