﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

namespace Hightlander.Pong.Gameplay
{
    /// <summary>
    ///     Игровое поле.
    /// </summary>
    public class Playfield
    {
        /// <summary>
        ///     Настройки и характеристики игрового поля.
        /// </summary>
        public PlayfieldSettings Settings { get; private set; }

        /// <summary>
        ///     Количество мячей на поле.
        /// </summary>
        public int BallsCount
        {
            get { return Balls.Count; }
        }

        private readonly List<Plank> Planks = new List<Plank>();
        private readonly List<Ball> Balls = new List<Ball>();

        /// <summary>
        ///     Событие изменения свойств игрового поля.
        /// </summary>
        public event Action PlayfieldChangedEvent;

        /// <summary>
        ///     Событие создания мяча.
        /// </summary>
        public event Action<Ball> BallSpawnedEvent;
        /// <summary>
        ///     Событие удаления мяча.
        /// </summary>
        public event Action<Ball> BallRemovedEvent;

        /// <summary>
        ///     Событие создания планки.
        /// </summary>
        public event Action<Plank> PlankSpawnedEvent;
        /// <summary>
        ///     Событие удаления планки.
        /// </summary>
        public event Action<Plank> PlankRemovedEvent;

        /// <summary>
        ///     Событие выхода мяча за границы игрового поля (поражение).
        /// </summary>
        public event Action<Ball> BallOutOfBoundsEvent;

        /// <summary>
        ///     Изменить конфигурацию игрового поля.
        /// </summary>
        /// <param name="settings">Настройки игрового поля.</param>
        [NotNull]
        public Playfield Configure(PlayfieldSettings settings)
        {
            Clear();

            Settings = settings;
            PlayfieldChangedEvent?.Invoke();

            return this;
        }

        /// <summary>
        ///     Получить список мячей на поле.
        /// </summary>
        [NotNull]
        public IReadOnlyList<Ball> GetBalls()
        {
            // Излишние выделения памяти из-за создании коллекции, можно оптимизировать.
            return new List<Ball>(Balls);
        }

        /// <summary>
        ///     Получить список планок на поле.
        /// </summary>
        [NotNull]
        public IReadOnlyList<Plank> GetPlanks()
        {
            // Излишние выделения памяти из-за создании коллекции, можно оптимизировать.
            return new List<Plank>(Planks);
        }

        /// <summary>
        ///     Добавить мяч на поле.
        /// </summary>
        /// <param name="ball">Новый мяч.</param>
        public void AddBall([NotNull] Ball ball)
        {
            if (ball == null)
            {
                throw new ArgumentNullException(nameof(ball));
            }

            if (Balls.Contains(ball))
            {
                throw new ArgumentException("Ball already on the playfield.", nameof(ball));
            }

            Balls.Add(ball);
            BallSpawnedEvent?.Invoke(ball);
        }

        /// <summary>
        ///     Удалить мяч с поля.
        /// </summary>
        /// <param name="ball">Мяч, который необходимо удалить.</param>
        public void RemoveBall([NotNull] Ball ball)
        {
            if (ball == null)
            {
                throw new ArgumentNullException(nameof(ball));
            }

            if (!Balls.Contains(ball))
            {
                throw new ArgumentException("Ball not exist on the playfield.", nameof(ball));
            }

            Balls.Remove(ball);
            BallRemovedEvent?.Invoke(ball);
        }

        /// <summary>
        ///     Добавить планку на поле.
        /// </summary>
        /// <param name="plank">Новая планка.</param>
        public Playfield AddPlank([NotNull] Plank plank)
        {
            if (plank == null)
            {
                throw new ArgumentNullException(nameof(plank));
            }

            if (Planks.Contains(plank))
            {
                throw new ArgumentException("Plank already on the playfield.", nameof(plank));
            }

            Planks.Add(plank);
            PlankSpawnedEvent?.Invoke(plank);

            return this;
        }

        /// <summary>
        ///     Удалить планку с поля.
        /// </summary>
        /// <param name="plank">Планка, которую необходимо удалить.</param>
        public Playfield RemovePlank([NotNull] Plank plank)
        {
            if (plank == null)
            {
                throw new ArgumentNullException(nameof(plank));
            }

            if (!Planks.Contains(plank))
            {
                throw new ArgumentException("Ball not exist on the playfield.", nameof(plank));
            }

            Planks.Remove(plank);
            PlankRemovedEvent?.Invoke(plank);

            return this;
        }

        public Vector3 TranslateToWorldCoordinates(Vector2 relativePosition, float z)
        {
            return new Vector3(relativePosition.x / Settings.PixelSize, -relativePosition.y / Settings.PixelSize, z);
        }

        /// <summary>
        ///     Обработать мяч, вышедший за границы игрового поля.
        /// </summary>
        public void ProcessBallOutOfBounds([NotNull] Ball ball)
        {
            BallOutOfBoundsEvent?.Invoke(ball);
        }

        /// <summary>
        ///     Очистить игровое поле.
        /// </summary>
        [NotNull]
        private void Clear()
        {
            List<Plank> PlanksCopy = new List<Plank>(Planks);
            foreach (Plank Plank in PlanksCopy)
            {
                RemovePlank(Plank);
            }

            List<Ball> BallsCopy = new List<Ball>(Balls);
            foreach (Ball Ball in BallsCopy)
            {
                RemoveBall(Ball);
            }
        }
    }
}