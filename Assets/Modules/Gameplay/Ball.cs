﻿using System;
using UnityEngine;

namespace Hightlander.Pong.Gameplay
{
    /// <summary>
    ///     Сущность мяча
    /// </summary>
    public class Ball
    {
        /// <summary>
        ///     Счётчик свободного идентификатора мяча.
        /// </summary>
        private static short UsedId;

        /// <summary>
        ///     Идентификатор мяча.
        /// </summary>
        public short Id { get; private set; }

        /// <summary>
        ///     Положение мяча.
        /// </summary>
        public Vector2 Position { get; set; }

        /// <summary>
        ///     Направление полёта мяча.
        /// </summary>
        public Vector2 Direction { get; set; }

        /// <summary>
        ///     Скорость полёта мяча.
        /// </summary>
        public float Speed { get; set; }

        /// <summary>
        ///     Радиус мяча.
        /// </summary>
        public float Radius { get; set; }

        /// <summary>
        ///     Цвет мяча.
        /// </summary>
        public Color Color { get; set; }

        /// <summary>
        ///     Флаг активности мяча.
        /// </summary>
        public bool Active { get; set; } = true;

        /// <summary>
        ///     Событие отскока от границы.
        /// </summary>
        public event Action<Ball> BorderCollisionEvent;

        /// <summary>
        ///     Событие отскока от планки.
        /// </summary>
        public event Action<Ball, Plank> PlankCollisionEvent;

        /// <summary>
        ///     Создать мяч с первым свободным идентификатором.
        /// </summary>
        public Ball()
        {
            GenerateId();
        }

        /// <summary>
        ///     Создать мяч с указанным идентификатором.
        /// </summary>
        /// <param name="id">Идентификатор мяча.</param>
        public Ball(short id)
        {
            Id = id;
        }

        /// <summary>
        ///     Создать идентификатор.
        /// </summary>
        private void GenerateId()
        {
            if (UsedId == short.MaxValue)
            {
                UsedId = short.MinValue;
            }

            Id = UsedId++;
        }

        /// <summary>
        ///     Отразить направление по оси Y.
        /// </summary>
        public void InvertDirectionX()
        {
            Direction = new Vector2(-Direction.x, Direction.y);
        }

        /// <summary>
        ///     Отразить направление по оси X.
        /// </summary>
        public void InvertDirectionY()
        {
            Direction = new Vector2(Direction.x, -Direction.y);
        }

        public void RaiseBorderCollisionEvent()
        {
            BorderCollisionEvent?.Invoke(this);
        }

        public void RaisePlankCollisionEvent(Plank plank)
        {
            PlankCollisionEvent?.Invoke(this, plank);
        }
    }
}