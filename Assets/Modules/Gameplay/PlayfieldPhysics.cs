﻿using System;
using System.Collections.Generic;
using Hightlander.Pong.Gameplay.DebugUtilities;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace Hightlander.Pong.Gameplay
{
    /// <summary>
    ///     Обработчик игровой физики.
    /// </summary>
    [UsedImplicitly(ImplicitUseKindFlags.InstantiatedNoFixedConstructorSignature)]
    public class PlayfieldPhysics : MonoBehaviour
    {
        [Inject, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private Playfield Playfield;

        [Inject, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private SessionController Session;

        private void Update()
        {
            if (!Session.IsSessionActive)
            {
                return;
            }

            RelativeDebugDrawer.DrawLine(new Vector2(0, 1), new Vector2(0, 0), Playfield.Settings.BordersSafety[0] ? Color.green : Color.red, Time.deltaTime, Playfield.Settings);
            RelativeDebugDrawer.DrawLine(new Vector2(0, 0), new Vector2(Playfield.Settings.AspectRatio, 0), Playfield.Settings.BordersSafety[1] ? Color.green : Color.red, Time.deltaTime, Playfield.Settings);
            RelativeDebugDrawer.DrawLine(new Vector2(Playfield.Settings.AspectRatio, 0), new Vector2(Playfield.Settings.AspectRatio, 1), Playfield.Settings.BordersSafety[2] ? Color.green : Color.red, Time.deltaTime, Playfield.Settings);
            RelativeDebugDrawer.DrawLine(new Vector2(Playfield.Settings.AspectRatio, 1), new Vector2(0, 1), Playfield.Settings.BordersSafety[3] ? Color.green : Color.red, Time.deltaTime, Playfield.Settings);

            IReadOnlyList<Ball> BallsCollection = Playfield.GetBalls();
            foreach (Ball Ball in BallsCollection)
            {
                if (!Ball.Active)
                {
                    continue;
                }

                ProcessBallStep(Playfield, Ball);
            }
        }

        /// <summary>
        ///     Обработать кадр перемещения мяча.
        /// </summary>
        /// <param name="playfield">Экземпляр игрового поля.</param>
        /// <param name="ball">Экземпляр мяча.</param>
        private static void ProcessBallStep([NotNull] Playfield playfield, [NotNull] Ball ball)
        {
            if (playfield == null)
            {
                throw new ArgumentNullException(nameof(playfield));
            }

            if (ball == null)
            {
                throw new ArgumentNullException(nameof(ball));
            }

            Vector2 Delta = ball.Direction * ball.Speed * Time.deltaTime;
            Vector2 CorrectedPosition = ball.Position + Delta;

            Border[] Borders =
            {
                new Border(0, 0, 1, 0),
                new Border(0, 0, 0, -1),
                new Border(playfield.Settings.AspectRatio, 1, -1, 0),
                new Border(playfield.Settings.AspectRatio, 1, 0, 1)
            };

            foreach (Plank Plank in playfield.GetPlanks())
            {
                ProcessPlankCollision(ball, Plank, playfield, ref CorrectedPosition);
            }

            bool CollidedWithBorders = false;
            for (int Index = 0; Index < Borders.Length; Index++)
            {
                Border Border = Borders[Index];
                Delta = CorrectedPosition - ball.Position;
                bool Collided = CheckCollision(ball, Delta, Border, out CorrectedPosition);
                CollidedWithBorders = CollidedWithBorders || Collided;

                if (Collided && !playfield.Settings.BordersSafety[Index])
                {
                    playfield.ProcessBallOutOfBounds(ball);
                    return;
                }
            }

            ball.Position = CorrectedPosition;

            if (CollidedWithBorders)
            {
                ball.RaiseBorderCollisionEvent();
            }
        }

        /// <summary>
        ///     Обработать возможность взаимодействия мяча с планкой.
        /// </summary>
        /// <param name="ball">Мяч</param>
        /// <param name="plank">Планка</param>
        /// <param name="playfield">Экземпляр игрового поля.</param>
        /// <param name="ballPosition">Положение мяча, входное и обработанное.</param>
        private static void ProcessPlankCollision([NotNull] Ball ball, [NotNull] Plank plank, [NotNull] Playfield playfield, ref Vector2 ballPosition)
        {
            if (ball == null)
            {
                throw new ArgumentNullException(nameof(ball));
            }

            if (plank == null)
            {
                throw new ArgumentNullException(nameof(plank));
            }

            if (playfield == null)
            {
                throw new ArgumentNullException(nameof(playfield));
            }

            Rect PlankRectangle = plank.GetRectangle();

            RelativeDebugDrawer.DrawLine(PlankRectangle.min, PlankRectangle.max, Color.red, Time.deltaTime, playfield.Settings);

            if (CheckCircleToRectangleCollision(ballPosition, ball.Radius, PlankRectangle))
            {
                Border PlankPseudoborder = new Border(plank.Position - plank.Normal * plank.Height / 2f, plank.Normal);

                RelativeDebugDrawer.DrawLine(new Vector2(0, PlankPseudoborder.Position.y),
                    new Vector2(playfield.Settings.AspectRatio, PlankPseudoborder.Position.y), Color.black, 2f,
                    playfield.Settings);

                Vector2 Delta = ballPosition - ball.Position;
                bool Collided = CheckCollision(ball, Delta, PlankPseudoborder, out ballPosition);

                if (Collided && plank.Owned)
                {
                    plank.RaiseBallCollisionEvent(ball);
                    ball.RaisePlankCollisionEvent(plank);
                }
            }
        }

        /// <summary>
        ///     Проверить коллизию мяча с границей поля.
        /// </summary>
        /// <param name="ball">Мяч</param>
        /// <param name="delta">Дельта перемещения мяча за шаг симуляции</param>
        /// <param name="border">Обрабатываемая граница.</param>
        /// <param name="correctedPosition">Скорректированная после коллизии позиция мяча.</param>
        /// <returns><c>true</c> - если был отскок, в ином случае - <c>false</c>.</returns>
        private static bool CheckCollision([NotNull] Ball ball, Vector2 delta, Border border, out Vector2 correctedPosition)
        {
            if (ball == null)
            {
                throw new ArgumentNullException(nameof(ball));
            }

            bool Collided = false;
            Vector2 NewPosition = ball.Position + delta;
            float CorrectedX = NewPosition.x;
            float CorrectedY = NewPosition.y;

            if (border.Normal.x != 0)
            {
                float NormalSign = Mathf.Sign(border.Normal.x);
                float BorderX = border.Position.x + NormalSign * ball.Radius;
                if (NormalSign * NewPosition.x < NormalSign * BorderX)
                {
                    float OutboundX = NormalSign * (BorderX - NewPosition.x);
                    CorrectedX = BorderX + NormalSign * OutboundX;
                    ball.InvertDirectionX();

                    Collided = true;
                }
            }

            if (border.Normal.y != 0)
            {
                float NormalSign = Mathf.Sign(border.Normal.y);
                float BorderY = border.Position.y - Mathf.Sign(border.Normal.y) * ball.Radius;
                if (NormalSign * NewPosition.y > NormalSign * BorderY)
                {
                    float OutboundY = NormalSign * (NewPosition.y - BorderY);
                    CorrectedY = BorderY - NormalSign * OutboundY;
                    ball.InvertDirectionY();

                    Collided = true;
                }
            }

            correctedPosition = new Vector2(CorrectedX, CorrectedY);
            return Collided;
        }


        /// <summary>
        ///     Проверить коллизию круга с прямоугольником.
        /// </summary>
        /// <param name="circlePosition">Положение круга.</param>
        /// <param name="radius">Радиус круга.</param>
        /// <param name="rectangle">Характеристики прямоугольника.</param>
        /// <returns><c>true</c> - если есть коллизия, в ином случае - <c>false</c>.</returns>
        private static bool CheckCircleToRectangleCollision(Vector2 circlePosition, float radius, Rect rectangle)
        {
            float ClosestX = Mathf.Clamp(circlePosition.x, rectangle.xMin, rectangle.xMax);
            float ClosestY = Mathf.Clamp(circlePosition.y, rectangle.yMin, rectangle.yMax);

            float DistanceX = circlePosition.x - ClosestX;
            float DistanceY = circlePosition.y - ClosestY;

            float DistanceSqr = (DistanceX * DistanceX) + (DistanceY * DistanceY);
            return DistanceSqr < (radius * radius);
        }

        /// <summary>
        ///     Сущность границы, от которой может отскочить мяч.
        /// </summary>
        private struct Border
        {
            public Vector2 Position;
            public Vector2 Normal;

            public Border(Vector2 position, Vector2 normal)
            {
                Position = position;
                Normal = normal;
            }

            public Border(float px, float py, float nx, float ny) : this(new Vector2(px, py), new Vector2(nx, ny))
            {
            }
        }
    }
}