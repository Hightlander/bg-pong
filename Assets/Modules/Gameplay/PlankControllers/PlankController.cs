﻿using System.Collections.Generic;
using JetBrains.Annotations;
using Zenject;

namespace Hightlander.Pong.Gameplay
{
    /// <summary>
    ///     Базовый контроллер управления планкой.
    /// </summary>
    public abstract class PlankController : IPlankController, ITickable
    {
        [Inject, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        protected Playfield Playfield;

        [Inject, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private TickableManager TickableManager;

        /// <summary>
        ///     Коллекция связанных с контроллером планок.
        /// </summary>
        protected readonly List<Plank> ConnectedPlanks = new List<Plank>();

        [Inject, UsedImplicitly]
        protected virtual void Setup()
        {
            TickableManager.Add(this);

            Playfield.PlankRemovedEvent += PlankRemovedEventHandler;
        }

        protected virtual void PlankRemovedEventHandler([NotNull] Plank plank)
        {
            if (ConnectedPlanks.Contains(plank))
            {
                ConnectedPlanks.Remove(plank);
            }
        }

        #region Implementation of IPlankController

        /// <inheritdoc />
        public virtual IPlankController ConnectPlank(Plank plank)
        {
            ConnectedPlanks.Add(plank);

            return this;
        }

        #endregion

        #region Implementation of IDisposable

        /// <inheritdoc />
        public virtual void Dispose()
        {
            TickableManager.Remove(this);
            ConnectedPlanks.Clear();
            Playfield.PlankRemovedEvent -= PlankRemovedEventHandler;
        }

        #endregion

        #region Implementation of ITickable

        /// <inheritdoc />
        public abstract void Tick();

        #endregion
    }
}