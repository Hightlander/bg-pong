﻿using UnityEngine;
using Zenject;

namespace Hightlander.Pong.Gameplay
{
    /// <summary>
    ///     Контроллер планки, управляемой мышкой\тачем.
    /// </summary>
    public class InputPlankController : PlankController
    {
        private bool CheatRegime = false;

        public override void Tick()
        {
            if (Input.GetKeyDown(KeyCode.H) && Input.GetKey(KeyCode.C))
            {
                CheatRegime = !CheatRegime;
            }

            float RelativeInputPosition = GetPlankPosition();
            foreach (Plank Plank in ConnectedPlanks)
            {
                Plank.Position = new Vector2(Mathf.Clamp(RelativeInputPosition, Plank.Width / 2f, Playfield.Settings.AspectRatio - Plank.Width / 2f), Plank.Position.y);
                if (CheatRegime)
                {
                    AIPlankController.ProcessPlank(Plank, Playfield);
                }
            }
        }

        private float GetPlankPosition()
        {
            float PlayfieldPixelWidth = Playfield.Settings.AspectRatio / Playfield.Settings.PixelSize;
            float PlayfieldToScreenDelta = Screen.width - PlayfieldPixelWidth;

            Vector2 InputPosition = Input.mousePosition;

            if (Input.touchSupported && Input.touchCount > 0)
            {
                var Touch = Input.GetTouch(0);

                InputPosition = Touch.position;
            }

            float InputPositionX = InputPosition.x - PlayfieldToScreenDelta / 2f;
            float RelativeInputPositionX = (InputPositionX / PlayfieldPixelWidth) * Playfield.Settings.AspectRatio;

            return RelativeInputPositionX;
        }
    }
}