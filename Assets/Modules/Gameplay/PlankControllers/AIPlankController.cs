﻿using UnityEngine;

namespace Hightlander.Pong.Gameplay
{
    /// <summary>
    ///     Контроллер управления планкой, который автоматически отбивает ближайшие мячи.
    /// </summary>
    public class AIPlankController : PlankController
    {
        internal static void ProcessPlank(Plank plank, Playfield playfield)
        {
            if (playfield.BallsCount <= 0)
            {
                return;
            }

            Ball NearestBall = null;
            float MinDistance = float.MaxValue;

            foreach (Ball Ball in playfield.GetBalls())
            {
                float Distance = Mathf.Abs(plank.Position.y - Ball.Position.y);
                if (NearestBall == null || Distance < MinDistance)
                {
                    NearestBall = Ball;
                    MinDistance = Distance;
                }
            }

            if (NearestBall != null)
            {
                plank.Position = new Vector2(Mathf.Clamp(NearestBall.Position.x, plank.Width / 2f, playfield.Settings.AspectRatio - plank.Width / 2f), plank.Position.y);
            }

        }

        #region Overrides of PlankController

        /// <inheritdoc />
        public override void Tick()
        {
            foreach (Plank Plank in ConnectedPlanks)
            {
                ProcessPlank(Plank, Playfield);
            }
        }

        #endregion
    }
}