﻿using System;
using JetBrains.Annotations;

namespace Hightlander.Pong.Gameplay
{
    public interface IPlankController : IDisposable
    {
        /// <summary>
        ///     Связать планку с контроллером.
        /// </summary>
        /// <param name="plank">Планка, которую требуется привязать.</param>
        [NotNull]
        IPlankController ConnectPlank([NotNull] Plank plank);
    }
}