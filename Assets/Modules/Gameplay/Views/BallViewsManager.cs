﻿using System;
using System.Collections.Generic;
using HightlanderSolutions.Utilities.Pooling;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace Hightlander.Pong.Gameplay
{
    /// <summary>
    ///     Менеджер вьюшек мячей.
    /// </summary>
    public class BallViewsManager : MonoBehaviour
    {
        [Inject, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private Playfield Playfield;

        [Inject, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private IInstantiator Instantiator;

        [SerializeField, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private GameObject Template;

        private readonly Dictionary<Ball, BallView> ActiveBallViews = new Dictionary<Ball, BallView>();

        private readonly PrefabInstancesPool<BallView> BallViewsPool = new PrefabInstancesPool<BallView>();

        [Inject, UsedImplicitly]
        private void Setup()
        {
            BallViewsPool.Initialize(Template, transform, Instantiator);
            Playfield.BallSpawnedEvent += BallSpawnedEventHandler;
            Playfield.BallRemovedEvent += BallRemovedEventHandler;

            Template.SetActive(false);
        }

        private void OnDestroy()
        {
            Playfield.BallSpawnedEvent -= BallSpawnedEventHandler;
            Playfield.BallRemovedEvent -= BallRemovedEventHandler;
        }

        private void Update()
        {
            foreach (Ball Ball in Playfield.GetBalls())
            {
                if (ActiveBallViews.ContainsKey(Ball))
                {
                    ActiveBallViews[Ball].SetPosition(Ball.Position);
                }
            }
        }

        private void BallSpawnedEventHandler([NotNull] Ball ball)
        {
            if (ball == null)
            {
                throw new ArgumentNullException(nameof(ball));
            }
            
            BallView View = BallViewsPool.GetInstance().Initialize(ball).SetColor(ball.Color);
            ActiveBallViews.Add(ball, View);
        }

        private void BallRemovedEventHandler([NotNull] Ball ball)
        {
            if (ball == null)
            {
                throw new ArgumentNullException(nameof(ball));
            }

            BallViewsPool.Release(ActiveBallViews[ball]);
            ActiveBallViews.Remove(ball);
        }
    }
}