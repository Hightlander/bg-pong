﻿using System;
using HightlanderSolutions.Utilities.Pooling;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Hightlander.Pong.Gameplay
{
    public class PlankView : MonoBehaviour, IPoolItem
    {
        [SerializeField, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private Image PlankImage;

        [Inject, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private Playfield Playfield;

        private Plank Target;

        private RectTransform InnerTransform;
        private RectTransform Transform
        {
            get
            {
                if (InnerTransform == null)
                {
                    InnerTransform = GetComponent<RectTransform>();
                }

                return InnerTransform;
            }
        }

        [NotNull]
        public PlankView Initialize([NotNull] Plank plank)
        {
            if (plank == null)
            {
                throw new ArgumentNullException(nameof(plank));
            }

            Target = plank;
            Transform.sizeDelta = new Vector2(plank.Width / Playfield.Settings.PixelSize, plank.Height / Playfield.Settings.PixelSize);
            gameObject.SetActive(true);

            plank.ColorChangedEvent += ColorChangedEventHandler;
            ColorChangedEventHandler(plank.Color);


            return this;
        }

        public PlankView SetPosition(Vector2 position)
        {
            Transform.localPosition = new Vector3(position.x / Playfield.Settings.PixelSize, -position.y / Playfield.Settings.PixelSize, Transform.localPosition.z);

            return this;
        }

        #region Implementation of IPoolItem

        /// <inheritdoc />
        public void Release()
        {
            gameObject.SetActive(false);
            Target.ColorChangedEvent -= ColorChangedEventHandler;
        }

        #endregion

        private void ColorChangedEventHandler(Color color)
        {
            PlankImage.color = color;
        }
    }
}