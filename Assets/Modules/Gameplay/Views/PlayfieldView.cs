﻿using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace Hightlander.Pong.Gameplay
{
    public class PlayfieldView : MonoBehaviour
    {
        [Inject, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private Playfield Playfield;

        private RectTransform RectTransform;

        [Inject, UsedImplicitly]
        private void Configure()
        {
            RectTransform = transform as RectTransform;
            Playfield.PlayfieldChangedEvent += PlayfieldChangedEventHandler;
        }

        private void OnDestroy()
        {
            Playfield.PlayfieldChangedEvent -= PlayfieldChangedEventHandler;
        }

        private void PlayfieldChangedEventHandler()
        {
            UpdateSizes();
        }

        private void UpdateSizes()
        {
            float Width = Playfield.Settings.AspectRatio / Playfield.Settings.PixelSize;
            float Height = 1f / Playfield.Settings.PixelSize;

            RectTransform.sizeDelta = new Vector2(Width, Height);
            transform.localPosition = new Vector2(-Width / 2f, Height / 2f);
        }
    }
}