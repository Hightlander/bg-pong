﻿using System;
using System.Collections.Generic;
using HightlanderSolutions.Utilities.Pooling;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace Hightlander.Pong.Gameplay
{
    public class PlankViewsManager : MonoBehaviour
    {
        [Inject, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private Playfield Playfield;


        [Inject, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private IInstantiator Instantiator;

        [SerializeField, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private GameObject Template;

        private readonly Dictionary<Plank, PlankView> ActivePlankViews = new Dictionary<Plank, PlankView>();

        private readonly PrefabInstancesPool<PlankView> PlankViewsPool = new PrefabInstancesPool<PlankView>();

        [Inject, UsedImplicitly]
        private void Setup()
        {
            PlankViewsPool.Initialize(Template, transform, Instantiator);
            Playfield.PlankSpawnedEvent += PlankSpawnedEventHandler;
            Playfield.PlankRemovedEvent += PlankRemovedEventHandler;

            Template.SetActive(false);
        }

        private void OnDestroy()
        {
            Playfield.PlankSpawnedEvent -= PlankSpawnedEventHandler;
            Playfield.PlankRemovedEvent -= PlankRemovedEventHandler;
        }

        private void Update()
        {
            foreach (Plank Plank in Playfield.GetPlanks())
            {
                if (ActivePlankViews.ContainsKey(Plank))
                {
                    ActivePlankViews[Plank].SetPosition(Plank.Position);
                }
            }
        }

        private void PlankSpawnedEventHandler([NotNull] Plank plank)
        {
            if (plank == null)
            {
                throw new ArgumentNullException(nameof(plank));
            }

            PlankView View = PlankViewsPool.GetInstance().Initialize(plank);
            ActivePlankViews.Add(plank, View);
        }

        private void PlankRemovedEventHandler([NotNull] Plank plank)
        {
            if (plank == null)
            {
                throw new ArgumentNullException(nameof(plank));
            }

            PlankViewsPool.Release(ActivePlankViews[plank]);
            ActivePlankViews.Remove(plank);
        }
    }
}