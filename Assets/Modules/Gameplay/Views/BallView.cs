﻿using System;
using HightlanderSolutions.Utilities.Pooling;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Hightlander.Pong.Gameplay
{
    [UsedImplicitly(ImplicitUseKindFlags.InstantiatedNoFixedConstructorSignature)]
    public class BallView : MonoBehaviour, IPoolItem
    {
        [Inject, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private Playfield Playfield;

        [SerializeField, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private Image BallImage;

        [SerializeField, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private TrailRenderer Trail;

        private RectTransform InnerTransform;
        private RectTransform Transform
        {
            get
            {
                if (InnerTransform == null)
                {
                    InnerTransform = GetComponent<RectTransform>();
                }

                return InnerTransform;
            }
        }

        private Ball Target;

        [NotNull]
        public BallView Initialize([NotNull] Ball ball)
        {
            Target = ball ?? throw new ArgumentNullException(nameof(ball));
            Target.BorderCollisionEvent += BorderCollisionEventHandler;

            Transform.sizeDelta = Vector3.one * ball.Radius * 2 / Playfield.Settings.PixelSize;
            SetPosition(ball.Position);
            Trail.Clear();
            const float PPU = 100;
            Trail.startWidth = ball.Radius * 2 / Playfield.Settings.PixelSize / PPU;
            gameObject.SetActive(true);

            return this;
        }

        public BallView SetPosition(Vector2 position)
        {
            Transform.localPosition = Playfield.TranslateToWorldCoordinates(position, Transform.localPosition.z);

            return this;
        }

        public BallView SetColor(Color color)
        {
            BallImage.color = color;
            Trail.startColor = color;
            Trail.endColor = new Color(color.r, color.g, color.b, 0);

            return this;
        }

        private void BorderCollisionEventHandler(Ball ball)
        {
            //Debug.Log("Border collided.", this);
        }

        #region Implementation of IPoolItem

        /// <inheritdoc />
        public void Release()
        {
            Trail.SetPositions(new Vector3[0]);
            gameObject.SetActive(false);
            Target.BorderCollisionEvent -= BorderCollisionEventHandler;
        }

        #endregion
    }
}