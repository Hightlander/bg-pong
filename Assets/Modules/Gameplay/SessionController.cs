﻿using System;
using JetBrains.Annotations;
using Zenject;

namespace Hightlander.Pong.Gameplay
{
    /// <summary>
    ///     Контроллер игровой сессии.
    /// </summary>
    public class SessionController
    {
        /// <summary>
        ///     Тип игровой сессии.
        /// </summary>
        public enum SessionTypes
        {
            /// <summary>
            ///     Одиночная игра.
            /// </summary>
            Singleplayer,

            /// <summary>
            ///     Многопользовательская игра.
            /// </summary>
            Multiplayer
        }

        [Inject, UsedImplicitly(ImplicitUseKindFlags.Assign)]
        private Playfield Playfield;

        private bool InnerIsSessionActive;

        /// <summary>
        ///     Флаг активности игровой сессии.
        /// </summary>
        public bool IsSessionActive
        {
            get => InnerIsSessionActive;
            set
            {
                if (InnerIsSessionActive != value)
                {
                    InnerIsSessionActive = value;
                    //Debug.Log($"Session state -> {value}");
                    SessionActivityChangedEvent?.Invoke(InnerIsSessionActive);
                }
            }
        }

        /// <summary>
        ///     Является ли игрок сервером в многопользовательской сессии.
        /// </summary>
        public bool IsServer { get; set; }

        /// <summary>
        ///     Тип игровой сессии.
        /// </summary>
        public SessionTypes Type { get; private set; }

        /// <summary>
        ///     Событие изменения активности игровой сессии.
        /// </summary>
        public event Action<bool> SessionActivityChangedEvent;

        /// <summary>
        ///     Событие начала игровой сессии.
        /// </summary>
        public event Action SessionStartedEvent;

        /// <summary>
        ///     Событие завершения игровой сессии.
        /// </summary>
        public event Action SessionEndedEvent;

        /// <summary>
        ///     Установить одиночный режим для сессии.
        /// </summary>
        [NotNull]
        public SessionController SetSingleplayerType()
        {
            Type = SessionTypes.Singleplayer;
            return this;
        }

        /// <summary>
        ///     Установить многопользовательский режим для сессии.
        /// </summary>
        /// <param name="isServer">Флаг показывающий является ли игрок сервером в многопользовательской игре.</param>
        [NotNull]
        public SessionController SetMultiplayerType(bool isServer)
        {
            Type = SessionTypes.Multiplayer;
            IsServer = isServer;
            return this;
        }

        /// <summary>
        ///     Начать игровую сессию.
        /// </summary>
        public void Start()
        {
            if (IsSessionActive)
            {
                return;
            }

            Playfield.BallRemovedEvent += BallRemovedEventHandler;
            
            IsSessionActive = true;
            SessionStartedEvent?.Invoke();
        }

        /// <summary>
        ///     Завершить игровую сессию.
        /// </summary>
        public void End()
        {
            if (!IsSessionActive)
            {
                return;
            }

            Playfield.BallRemovedEvent -= BallRemovedEventHandler;

            IsSessionActive = false;
            SessionEndedEvent?.Invoke();
        }

        private void BallRemovedEventHandler(Ball ball)
        {
            if (Playfield.BallsCount <= 0)
            {
                End();
                Start();
            }
        }
    }
}